# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.0] - 2025-03-05

### Fixed

- Fixed security headers cache for "/api"

## [1.1.9] - 2025-02-28

### Fixed

- Fixed form-action security headers ([#139](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/139))

## [1.1.8] - 2025-02-28

### Added

-  Security headers ([#139](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/139))

## [1.1.7] - 2024-11-27

### Fixed

- Fixed error logging ([#19])(https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend/-/issues/19))

## [1.1.6] - 2024-10-31

### Added

-   Instructions page ([#131](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/131))

## [1.1.5] - 2024-10-29

### Security

-   Update next to fix security vulnerabilities

## [1.1.4] - 2024-09-05

### Fixed

- Fixed topline taxt from keyword to theme ([#18](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend/-/issues/18))

## [1.1.3] - 2024-08-19

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.1.2] - 2024-08-16

### Changed

- next-auth middleware

## [1.1.1] - 2024-08-01

### Fixed

- Fixed signin signout callback urls and logic ([#128](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/128))

## [1.1.0] - 2024-07-08

### Added

- User & organization management for `superadmin` users
- Possibility to change the owner of a dataset

## [1.0.6] - 2024-04-12

### Changed

- update to next v14 ([#120](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/120))

## [1.0.5] - 2023-11-16

### Changed

- pagination improved, works same as in lkod catalog ([#115](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/115))

## [1.0.4] - 2023-10-23

### Changed

- Footer redesigned

## [1.0.3] - 2023-09-26

### Added

- Add server and client side logging with pino ([#12](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend/-/issues/12))

### Changed

- Api calls refactored to be unified with other Golemio apps ([#11](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend/-/issues/11))

## [1.0.2] - 2023-07-17

### Fixed

- Fixed error 500 when search doesn't find any results ([#10](https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend/-/issues/10))

## [1.0.1] - 2023-06-27

### Changed

- New oict logo

## [1.0.0] - 2023-06-08

### Added

- Add `CHANGELOG.md` file

### Changed

- Datasets page and dataset card reacts on whether the data is from ArcGIS or not with disabled button functionality, different styling and notification (information is taken from dataset parameter `isReadOnly`) ([#98](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/98))

