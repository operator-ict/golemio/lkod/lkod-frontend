ARG BASE_IMAGE=node:20.18.0-alpine

FROM $BASE_IMAGE AS builder
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
COPY .env.template ./.env
RUN npm run build


FROM $BASE_IMAGE
WORKDIR /app

# Create a non-root user
RUN addgroup -S nonroot && \
    adduser -S nonroot -G nonroot -h /app -u 1001 -D && \
    chown -R nonroot /app

COPY --from=builder /app/public ./public
COPY --from=builder /app/next.config.js ./next.config.js
COPY --from=builder /app/.env.template ./.env
COPY --from=builder --chown=nonroot:nonroot /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json

USER nonroot

EXPOSE 3000
CMD ["npm", "run", "start"]
