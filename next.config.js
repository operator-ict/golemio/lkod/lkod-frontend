/* eslint-disable max-len */

/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    images: {
        remotePatterns: [
            { protocol: "https", hostname: "rabingolemio.blob.core.windows.net" },
            { protocol: "https", hostname: "golemgolemio.blob.core.windows.net" },
            { protocol: "https", hostname: "img.icons8.com" },
            { protocol: "https", hostname: "publications.europa.eu" },
        ],
        minimumCacheTTL: 120,
        deviceSizes: [828, 1200],
        formats: ["image/webp"],
    },
    i18n: {
        locales: ["cs-CZ"],
        defaultLocale: "cs-CZ",
    },
    logging: {
        fetches: {
            fullUrl: true,
        },
    },

    async redirects() {
        return [
            {
                source: "/login",
                has: [
                    {
                        type: "cookie",
                        key: "next-auth.session-token",
                        value: "true",
                    },
                ],
                permanent: true,
                destination: "/",
            },
        ];
    },
    async headers() {
        return [
            {
                source: "/:path*",
                headers: [
                    {
                        key: "X-DNS-Prefetch-Control",
                        value: "on",
                    },
                    {
                        key: "Strict-Transport-Security",
                        value: "max-age=31536000; includeSubDomains; preload",
                    },
                    {
                        key: "X-Frame-Options",
                        value: "SAMEORIGIN",
                    },
                    {
                        key: "X-Content-Type-Options",
                        value: "nosniff",
                    },
                    {
                        key: "Referrer-Policy",
                        value: "strict-origin-when-cross-origin",
                    },
                    {
                        key: "Permissions-Policy",
                        value: "camera=(), geolocation=(), fullscreen=(self)",
                    },
                    {
                        key: "X-XSS-Protection",
                        value: "1; mode=block",
                    },
                ],
            },
            {
                // settings for authentication routes
                source: "/api/auth/:path*",
                headers: [
                    {
                        key: "Content-Security-Policy",
                        value: "default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; connect-src 'self' https: http:; style-src 'self' 'unsafe-inline'; img-src 'self' data: https:; font-src 'self' data:; frame-src 'self' https:;",
                    },
                    {
                        key: "Cache-Control",
                        value: "no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0",
                    },
                    {
                        key: "Pragma",
                        value: "no-cache",
                    },
                ],
            },
            {
                source: "/((?!api/auth).*)",
                headers: [
                    {
                        key: "Content-Security-Policy",
                        value: `default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://static.cloudflareinsights.com https://*.cloudflare.com; connect-src 'self' https://stats.g.doubleclick.net https://login.microsoftonline.com https://*.cloudflare.com https://cloudflareinsights.com ${process.env.NEXTAUTH_URL}  https://data.gov.cz; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; img-src 'self' data: blob: https:; font-src 'self' data: https://fonts.gstatic.com; frame-src 'self' https://login.microsoftonline.com; form-action 'self' https://data.gov.cz; `,
                    },
                    {
                        key: "Cross-Origin-Opener-Policy",
                        value: "same-origin",
                    },
                    {
                        key: "Cross-Origin-Resource-Policy",
                        value: "same-origin",
                    },
                    {
                        key: "Expect-CT",
                        value: "max-age=86400, enforce",
                    },
                ],
            },
        ];
    },
};

module.exports = nextConfig;
