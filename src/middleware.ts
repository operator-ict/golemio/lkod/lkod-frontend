import { NextRequest, NextResponse } from "next/server";
import { getToken } from "next-auth/jwt";
import { validateSessionToken } from "@/middleware/validateSessionToken";
export { default } from "next-auth/middleware";
const secret = process.env.NEXTAUTH_SECRET;
export const config = {
    matcher: ["/", "/users", "/organizations", "/organizations/:path*", "/change-password"],
};
export async function middleware(req: NextRequest) {
    const { url } = req;
    const sessionToken = await getToken({ req, secret });
    const signInUrl = new URL("/login", url);
    const signOutUrl = new URL("/signout", url);
    signInUrl.searchParams.set("from", req.nextUrl.pathname);
    const isUsersPage = url.includes("/users");
    const organizationsPage = url.includes("/organizations");
    if (!sessionToken) {
        return NextResponse.redirect(signInUrl);
    }
    try {
        const { isSuperAdmin, isExpired } = await validateSessionToken(sessionToken.token, sessionToken.role as string);
        if (isExpired) {
            signOutUrl.searchParams.set("expired", "true");
            return NextResponse.redirect(signOutUrl);
        }
        if (!isSuperAdmin && (isUsersPage || organizationsPage)) {
            return NextResponse.rewrite(new URL("/not-found", url));
        }
    } catch (error) {
        return NextResponse.redirect(signInUrl);
    }
    return NextResponse.next();
}
