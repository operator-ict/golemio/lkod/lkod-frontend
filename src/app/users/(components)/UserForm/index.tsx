"use client";
import React, { ChangeEventHandler, SyntheticEvent, useState } from "react";

import Button from "@/components/Button";
import { Select } from "@/components/Select";
import TextField from "@/components/TextField";
import { ResponseType } from "@/services/api-client";
import { UserRole } from "@/services/authorization-service";
import { User } from "@/services/users-service";
import text from "@/textContent/cs.json";
import styles from "@/users/(components)/UserForm/UserForm.module.scss";
import { addUser, updateUser } from "@/users/actions";
import getRoleLabel from "@/utils/roles";

interface UserFormProps {
    toUpdate?: User;
    onSaved: () => void;
}

const UserForm = (props: UserFormProps) => {
    const { toUpdate } = props;

    const [status, setStatus] = useState<undefined | null | string>(undefined);
    const [fields, setFields] = useState({
        name: toUpdate?.name ?? "",
        email: toUpdate?.email ?? "",
        role: toUpdate?.role ?? UserRole.user,
    });

    const refreshOrThrow = (response: ResponseType<User>) => {
        if (response.status === 200 || response.status == 201) {
            props.onSaved();
        } else {
            throw { status: response.status };
        }
    };

    const onSubmit = async (e: SyntheticEvent) => {
        e.preventDefault();
        setStatus(null);
        try {
            let response: ResponseType<User>;
            if (toUpdate) {
                response = await updateUser(toUpdate.id, fields);
            } else {
                response = await addUser(fields);
            }
            refreshOrThrow(response);
            setStatus(undefined);
        } catch (error) {
            setStatus(getErrorKey(error.status));
        }
    };

    const getErrorKey = (reason: number | string) => {
        switch (reason) {
            case 400:
                return `${text.errors.validationFailed}`;
            case 409:
                return `${text.errors.duplicateEmail}`;
            default:
                return `${text.errors.unknown}`;
        }
    };

    const onOptionChangeHandler: ChangeEventHandler<HTMLSelectElement> = (e) => {
        setFields((prevState) => ({
            ...prevState,
            role: e.target.value as UserRole,
        }));
    };

    return (
        <form className={styles["user-form"]} onSubmit={onSubmit}>
            <TextField
                name="name"
                type="text"
                placeholder={text.users.form.namePrompt}
                label={text.users.form.name}
                value={fields.name}
                onChange={(e) =>
                    setFields((prevState) => ({
                        ...prevState,
                        name: e.target.value,
                    }))
                }
            />
            <TextField
                name="email"
                type="email"
                placeholder={text.users.form.emailPrompt}
                label={text.users.form.email}
                value={fields.email}
                onChange={(e) =>
                    setFields((prevState) => ({
                        ...prevState,
                        email: e.target.value,
                    }))
                }
            />
            <Select label={text.users.form.role} onChange={onOptionChangeHandler} value={fields.role}>
                {Object.values(UserRole).map((option) => {
                    return (
                        <option key={option} value={option}>
                            {getRoleLabel(option)}
                        </option>
                    );
                })}
            </Select>
            <div className={`${styles["input-button"]}`}>
                <Button color="secondary" type="submit" label={toUpdate ? text.change : text.create} disabled={status === null} />
                <div className={`${styles["error"]}`}>{status}</div>
            </div>
        </form>
    );
};

export default UserForm;
