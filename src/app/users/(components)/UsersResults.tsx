import React from "react";

import { PageNavigation } from "@/components/PageNavigation";
import { ResultLine } from "@/components/ResultLine";
import { ApiClient } from "@/services/initApi";
import { User } from "@/services/users-service";
import text from "@/textContent/cs.json";
import { EditableUserList } from "@/users/(components)/EditableUserList";
import { UsersProps } from "@/users/page";
import styles from "@/users/Users.module.scss";
import { paginate } from "@/utils/pagination";
import { userMatchesQuery } from "@/utils/search";

const ORG_PER_PAGE_COUNT = 10;

const UsersResults = async ({ searchParams }: UsersProps) => {
    const { page, perpage, search } = searchParams;
    const { usersApi } = ApiClient;

    const session = await usersApi.getCurrentUser();
    const currentUserId = session.data?.id ?? -1;

    const response = await usersApi.getAllUsers();
    const users = response.data ?? [];

    const filtered = filter(users, search);
    const paginated = paginate(filtered, page, perpage, ORG_PER_PAGE_COUNT);
    return (
        <>
            <ResultLine
                result={filtered.length}
                foundPlurals={text.users.search.foundPlurals}
                plurals={text.users.search.plurals}
            />
            <div className={styles.usersContainer}>
                <EditableUserList users={paginated} currentUserId={currentUserId} />
            </div>
            <div className={styles.navigation}>
                <PageNavigation defaultPerPage={ORG_PER_PAGE_COUNT} page={page} perpage={perpage} count={filtered.length} />
            </div>
        </>
    );
};

const filter = (users: User[], search: string | undefined) => {
    const query = search ?? "";
    return users?.filter((usr) => userMatchesQuery(usr, query));
};

export default UsersResults;
