import { NextRequest, NextResponse } from "next/server";
import { getServerSession } from "next-auth";
import { config } from "src/config";

import { getLogger } from "@/logging/logger";

import { getErrorMessage } from "../../(utils)/getErrorMessage";
import { authOptions } from "../auth/[...nextauth]/authOptions";

const logger = getLogger("form-data-route");

export async function POST(request: NextRequest) {
    try {
        const session = await getServerSession(authOptions);
        if (!session?.token) {
            logger.warn({
                provider: "server",
                component: "Route Handler-form-data",
                message: "Unauthorized request: No session token found",
            });
            return NextResponse.json({ success: false, error: "Unauthorized" }, { status: 401 });
        }

        const data = await request.formData();

        const file = data.get("datasetFile") as File | null;
        const url = data.get("path") as string;

        if (!file || !url) {
            logger.warn({
                provider: "server",
                component: "Route Handler-form-data",
                message: "Missing required form data: datasetFile or path",
            });
            return NextResponse.json({ success: false, error: "Missing required form data" }, { status: 400 });
        }

        const formData = new FormData();
        formData.append("datasetFile", file);

        const fetchOptions = {
            method: "POST",
            headers: {
                Authorization: `Bearer ${session.token}`,
            },
            body: formData,
        };

        const apiResponse = await fetch(`${config.BACKEND_URL}/${url}`, fetchOptions);
        const responseData = await apiResponse.json();

        if (apiResponse.ok) {
            logger.debug({
                provider: "server",
                component: "Route Handler-form-data",
                message: `File uploaded successfully to ${url}`,
                status: apiResponse.status,
            });
            return NextResponse.json({ success: true, result: responseData });
        } else {
            logger.error({
                provider: "server",
                component: "Route Handler-form-data",
                /* eslint-disable max-len */
                message: `Failed to upload file: status ${apiResponse.status} | message: ${responseData?.message || responseData}`,
            });
            return NextResponse.json({ success: false, error: "Failed to upload file" }, { status: apiResponse.status });
        }
    } catch (error) {
        logger.error({
            provider: "server",
            component: "Route Handler-form-data",
            status: error.status,
            message: getErrorMessage(error),
        });

        return NextResponse.json({ success: false, error: error.message }, { status: error.status });
    }
}
