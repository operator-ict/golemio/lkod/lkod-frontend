export interface RawDatasetsItem {
    iri: string;
    title: string;
    description?: string;
    publisher?: string;
    rawThemes?: string;
    rawFormats?: string;
}

export interface DatasetsItem {
    iri: string;
    title: string;
    description: string | null;
    publisher: string | null;
    themeNames: string[] | null;
    formats: string[] | null;
    themeImage: string | null;
}

export interface RawDataset {
    iri: string;
    title: string;
    description?: string;
    publisherIri?: string;
    publisher?: string;
    publisherLogo?: string;
    publisherDesc?: string;
    documentation?: string;
    specification?: string;
    accrualPeriodicity?: string;
    temporalFrom?: string;
    temporalTo?: string;
    temporalResolution?: string;
    spatialResolutionInMeters?: string;
    contactPointName?: string;
    contactPointEmail?: string;
}

export interface Dataset {
    iri: string;
    title: string;
    description: string | null;
    publisher: {
        iri: string;
        title: string;
        logo: string | null;
        description: string | null;
    } | null;
    documentation: string | null;
    specification: string | null;
    accrualPeriodicity: string | null;
    temporalFrom: string | null;
    temporalTo: string | null;
    temporalResolution: string | null;
    spatialResolutionInMeters: string | null;
    contactPoint: {
        name: string;
        email: string;
    } | null;
    keywords: string[];
    themes: Array<{
        iri: string;
        title: string;
        image: string;
    }>;
    eurovocThemes: string[];
    distributions: {
        files: Array<{
            distributionIri: string;
            title: string;
        }>;
        services: Array<{
            distributionIri: string;
            accessServiceIri: string;
            title: string;
        }>;
    };
    spatial: string[];
}
