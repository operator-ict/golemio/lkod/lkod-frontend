"use client";
import React, { FC, useState } from "react";

import { NewDatasetDialog } from "../(components)/NewDatasetDialog/newDatasetDialog";
import { Organization } from "../(services)/organizations-service";

interface FilesDialogWrapperProps {
    organizations: Organization[];
    onClick?: () => void;
}

export const FilesDialogWrapper: FC<FilesDialogWrapperProps> = ({ organizations }) => {
    const [isOpen, setIsOpen] = useState(false);

    const handleClick = () => {
        setIsOpen(true);
    };

    return (
        <div>
            <div onClick={handleClick}>
                <div>Click to open dialog</div>
                <NewDatasetDialog closeDialog={() => setIsOpen(false)} isOpen={isOpen} organizations={organizations} />
            </div>
        </div>
    );
};
