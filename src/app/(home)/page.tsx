import { getServerSession } from "next-auth";
import React, { FC, Suspense } from "react";

import Filters from "@/(modules)/filters/Filters";
import FilterView from "@/(modules)/filters/FilterView";
import { SearchForm } from "@/(modules)/search/SearchForm";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { ApiClient } from "@/services/initApi";
import text from "@/textContent/cs.json";
import { ValidatedDataset } from "@/types/ValidatedDatasetInterface";
import { stringOrArr } from "@/utils/helpers";

import { NoResult } from "../(components)/NoResult";
import { Spinner } from "../(components)/Spinner";
import DataProvider from "../(context)/DataProvider";
import { Organization } from "../(services)/organizations-service";
import { authOptions } from "../api/auth/[...nextauth]/authOptions";
import FromArcGis from "./(IsFromArcGis)";
import DatasetsListCard from "./DatasetsListCard";
import styles from "./Home.module.scss";

const DATASET_PER_PAGE_COUNT = 6;
type Params = {
    searchParams: {
        page: string;
        publisherIri: string;
        themeIris: string | string[];
        keywords: string | string[];
        formatIris: string | string[];
        search: string;
        perpage: string;
        status: string | string[];
        count: number;
    };
};

const Datasets: FC<Params> = async ({ searchParams }) => {
    const { datasetsApi, organizationsApi, usersApi } = ApiClient;
    const { page, publisherIri, themeIris, keywords, formatIris, search, perpage, status } = searchParams;
    const session = await getServerSession(authOptions);
    const users = (await usersApi.getAllUsers()).data ?? [];

    const filter = {
        status: status ? stringOrArr(status) : [],
        publisherIri: publisherIri ? publisherIri : "",
        themeIris: themeIris ? stringOrArr(themeIris) : [],
        keywords: keywords ? stringOrArr(keywords) : [],
        formatIris: formatIris ? stringOrArr(formatIris) : [],
    };
    const searchString = search || "";
    const limit = perpage ? +perpage : DATASET_PER_PAGE_COUNT;
    const offset = page ? (+page - limit / DATASET_PER_PAGE_COUNT) * DATASET_PER_PAGE_COUNT : 0;

    const responseDatasets = await datasetsApi.getFilteredDatasets(
        { filter, searchString, limit, offset },
        { Authorization: session?.token || "" }
    );
    const orgData = await organizationsApi.getAllOrganization();

    const datasets: ValidatedDataset[] = Array.isArray(responseDatasets.data)
        ? (responseDatasets.data as ValidatedDataset[])
        : [];
    const publishersPromise = datasetsApi.getPublishers(filter);
    const themesPromise = datasetsApi.getThemes(filter);
    const keywordsPromise = datasetsApi.getKeywords(filter);
    const formatsPromise = datasetsApi.getFormats(filter);
    const statusesPromise = datasetsApi.getStatusesDataset(filter);

    const count = parseInt(responseDatasets.count as string, 10);
    const isFromArcGis = Array.isArray(datasets) && datasets.length > 0 && datasets[0].isReadOnly;

    return (
        <DataProvider
            publishersPromise={publishersPromise}
            themesPromise={themesPromise}
            keywordsPromise={keywordsPromise}
            formatsPromise={formatsPromise}
            statusesPromise={statusesPromise}
        >
            <Container>
                <div className={styles["top-container"]}>
                    <Heading tag={`h1`}>{text.configurable.siteTitle}</Heading>
                    <FromArcGis isFromArcGis={isFromArcGis} organizations={orgData.data as Organization[]} />
                </div>
                <SearchForm label={`Vyhledávání`} fullWidth />
                <div className={styles["datasets-container"]}>
                    <Suspense fallback={<Spinner />}>
                        <Filters label={text.filters.title} publisherIri={publisherIri} />
                        <FilterView />
                    </Suspense>
                    <div className={styles["datasets-list"]}>
                        <Suspense fallback={<Spinner />}>
                            <DatasetsListCard count={count} datasets={datasets} searchParams={searchParams} users={users} />
                            {+count === 0 && <NoResult />}
                        </Suspense>
                    </div>
                </div>
            </Container>
        </DataProvider>
    );
};

export default Datasets;
