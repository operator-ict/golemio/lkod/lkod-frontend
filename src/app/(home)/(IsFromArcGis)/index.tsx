"use client";
import { FC, useState } from "react";

import Button from "@/components/Button";
import ButtonInfo from "@/components/ButtonInfo";
import { DownCloudIcon } from "@/components/icons/DownCloudIcon";
import { FolderPlusIcon } from "@/components/icons/FolderPlusIcon";

import { NewDatasetDialog } from "../../(components)/NewDatasetDialog/newDatasetDialog";
import { Organization } from "../../(services)/organizations-service";
import styles from "../Home.module.scss";

interface IsFromArcGisProps {
    isFromArcGis: boolean;
    organizations: Organization[] | null;
}
const FromArcGis: FC<IsFromArcGisProps> = ({ isFromArcGis, organizations }) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [addNewOpen, setAddNewOpen] = useState(false);

    return (
        <div>
            {isFromArcGis ? (
                <ButtonInfo infoText={`Organizace stahuje své datasety z ArcGiSu`}>
                    <DownCloudIcon color="primary" />
                </ButtonInfo>
            ) : (
                <Button
                    color={`secondary`}
                    label={`Přidat datovou sadu`}
                    className={styles["add-dataset"]}
                    onClick={() => {
                        setAddNewOpen(true);
                        window.scrollTo({ top: 0, behavior: "smooth" });
                    }}
                >
                    <FolderPlusIcon color="tertiary" />
                </Button>
            )}
            <NewDatasetDialog
                closeDialog={() => setAddNewOpen(false)}
                isOpen={addNewOpen}
                organizations={organizations as Organization[]}
            />
        </div>
    );
};

export default FromArcGis;
