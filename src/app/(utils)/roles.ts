import { UserRole } from "@/services/authorization-service";

const getRoleLabel = (role: UserRole): string => {
    if (role === UserRole.user) {
        return "Uživatel | Spravuje pouze jím vlastněné datové sady";
    } else if (role === UserRole.admin) {
        return "Uživatel | Spravuje všechny datové sady svých organizací";
    } else if (role === UserRole.superadmin) {
        return "Administrátor | Spravuje všechna data v systému";
    } else {
        return role;
    }
};

export default getRoleLabel;
