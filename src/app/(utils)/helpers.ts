import { IFilter } from "@/types/FilterInterface";

export const stringOrArr = (data: string | string[] | undefined) => {
    if (!data) {
        return [];
    } else return typeof data === "string" ? [data] : [...data];
};

export const capitalizeFirstLetter = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
};

export const toQueryString = (filter: IFilter) => {
    let string = "?";

    filter.status && filter.status.length > 0
        ? Array.isArray(filter.status) &&
          filter.status.map((item) => {
              string = string.concat(`status[]=${item}&`);
          })
        : "";
    filter.publisherIri ? (string = string.concat(`publisherIri=${filter.publisherIri}&`)) : "";
    filter.themeIris && filter.themeIris.length > 0
        ? filter.themeIris.map((item) => {
              string = string.concat(`themeIris[]=${item}&`);
          })
        : "";
    filter.keywords && filter.keywords.length > 0
        ? filter.keywords.map((item) => {
              string = string.concat(`keywords[]=${item}&`);
          })
        : "";
    filter.formatIris && filter.formatIris.length > 0
        ? filter.formatIris.map((item) => {
              string = string.concat(`formatIris[]=${item}&`);
          })
        : "";

    return string;
};

export function transformToFilesArray(input: unknown): string[] {
    if (typeof input === "object" && input !== null && Array.isArray((input as { files?: unknown }).files)) {
        return (input as { files: string[] }).files;
    }
    return [];
}
