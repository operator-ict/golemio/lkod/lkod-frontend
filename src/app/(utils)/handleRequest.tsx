import { ApiClient } from "@/(services)/api-client";
import { RequestInit } from "@/(services)/client/datasets-service-client";

import { getLogger } from "../(logging)/logger";
import { getErrorMessage } from "./getErrorMessage";
const logger = getLogger("datasets-service-route");

export async function handleRequest(request: Request) {
    try {
        const clientRouteRequestParams: RequestInit = await request.json();
        const { method, requestUrl, body, headers } = clientRouteRequestParams;
        if (!clientRouteRequestParams) {
            throw new Error("Request parameters are missing or invalid.");
        }
        const res = await ApiClient.request(method, requestUrl, body, headers);
        logger.debug({ provider: "server", component: "handleRequest", message: `Request: ${method}/${requestUrl.toString()}` });

        if (res.status === 200 || res.status < 300) {
            return Response.json(res);
        } else {
            throw new Error(`Failed to fetch ${requestUrl}`);
        }
    } catch (err) {
        logger.error({ provider: "server", component: "handleRequest", message: getErrorMessage(err) });
        return new Response(JSON.stringify({ success: false, error: "Unexpected Error" }), {
            status: err.status,
        });
    }
}
export async function handleRequestGet(request: Request) {
    try {
        const url = new URL(request.url);
        const { search } = url;
        const requestUrl = search.toString().substring(1);
        logger.debug({ provider: "server", component: "handleRequestGet", message: `Request: ${requestUrl}` });

        const res = await ApiClient.get(requestUrl, request.headers);
        logger.debug({
            provider: "server",
            component: "handleRequestGet",
            message: "Response status:",
            status: res.status,
            data: res.data,
            error: res.error,
        });

        if (res.status === 200 || res.status < 300) {
            return Response.json(res.data);
        } else {
            throw new Error(`Request failed with status ${res.status}`);
        }
    } catch (err) {
        logger.error({
            provider: "server",
            component: "handleRequestGet",
            message: "Request failed",
            error: getErrorMessage(err),
            status: err.status || 500,
        });

        return Response.json({ success: false, error: getErrorMessage(err) }, { status: err.status || 500 });
    }
}
