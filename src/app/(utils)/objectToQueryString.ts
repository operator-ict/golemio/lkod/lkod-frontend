import { genericFilter, IFilter } from "@/(types)/FilterInterface";

export const objectToQueryString = (obj: genericFilter & IFilter) => {
    const pairs = Object.entries(obj).flatMap(([key, value]) => {
        if (Array.isArray(value)) {
            return value.map((item) => `${encodeURIComponent(key)}=${encodeURIComponent(String(item))}`);
        } else {
            return `${encodeURIComponent(key)}=${encodeURIComponent(String(value))}`;
        }
    });

    return pairs.join("&");
};
