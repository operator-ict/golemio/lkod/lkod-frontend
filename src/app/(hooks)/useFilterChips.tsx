"use client";
/* eslint-disable react-hooks/exhaustive-deps */
import { use, useMemo } from "react";

import { IListFormat, IListKeyword, IListPublisher, IListTheme } from "@/(schema)";
import {
    useFormatsPromise,
    useKeywordsPromise,
    usePublishersPromise,
    useStatusesPromise,
    useThemesPromise,
} from "@/context/DataProvider";

import { IStatus } from "../(types)/LookupTypes";
import { useUrlFilter } from "./useUrlFilter";

export const useFilterChips = () => {
    const publishersData = use(usePublishersPromise());
    const topicsData = use(useThemesPromise());
    const keywordsData = use(useKeywordsPromise());
    const formatsData = use(useFormatsPromise());
    const statusesData = use(useStatusesPromise());
    const { urlFilter } = useUrlFilter();

    const { publisherIri, themeIris, keywords, formatIris, status } = urlFilter;

    return useMemo(() => {
        // Find publisher
        const publishers = publisherIri
            ? ([publishersData.find((element: IListPublisher) => element.iri === publisherIri)].filter(
                  Boolean
              ) as IListPublisher[])
            : [];

        // Find topics
        const topics = themeIris
            ? ((Array.isArray(themeIris)
                  ? themeIris.map((iri) => topicsData.find((topic) => topic.iri === iri))
                  : [topicsData.find((topic) => topic.iri === themeIris)]
              ).filter(Boolean) as IListTheme[])
            : [];

        // Find keywords
        const keys = keywords
            ? ((Array.isArray(keywords)
                  ? keywords.map((key) => keywordsData.find((keyword) => keyword.label === key))
                  : [keywordsData.find((keyword) => keyword.label === keywords)]
              ).filter(Boolean) as IListKeyword[])
            : [];

        // Find formats
        const formats = formatIris
            ? ((Array.isArray(formatIris)
                  ? formatIris.map((iri) => formatsData.find((format) => format.iri === iri))
                  : [formatsData.find((format) => format.iri === formatIris)]
              ).filter(Boolean) as IListFormat[])
            : [];

        // Find statuses
        const statuses = status
            ? ((Array.isArray(status)
                  ? status.map((s) => statusesData.find((st) => st.label === s))
                  : [statusesData.find((st) => st.label === status)]
              ).filter(Boolean) as IStatus[])
            : [];

        return { topics, publishers, keys, formats, statuses };
    }, [
        publisherIri,
        themeIris,
        keywords,
        formatIris,
        status,
        publishersData,
        topicsData,
        keywordsData,
        formatsData,
        statusesData,
    ]);
};
