"use client";
import React, { createContext, FC, ReactNode, useState } from "react";

const MESSAGE_TIMEOUT = 15000;

export enum NotificationType {
    ERROR,
    SUCCESS,
}

interface Notification {
    id: number;
    message: ReactNode;
    timer?: NodeJS.Timeout;
    type: NotificationType;
}

interface NotificationsContextType {
    addNotification: (message: ReactNode, type: NotificationType, timeout?: number) => void;
}

const NotificationsContext = createContext<NotificationsContextType>({
    addNotification: () => {},
});

export const NotificationsProvider: FC<{ children: ReactNode }> = ({ children }) => {
    const [notifications, setNotifications] = useState<Notification[]>([]);
    const [nextId, setNextId] = useState(0);

    const deleteNotification = (notificationId: number) => {
        setNotifications((previousNotifications) => {
            const notificationToDelete = previousNotifications.find(({ id }) => id === notificationId);

            if (notificationToDelete?.timer) {
                clearTimeout(notificationToDelete.timer);
            }

            return [...previousNotifications.filter(({ id }) => id !== notificationId)];
        });
    };

    const addNotification = (message: ReactNode, type: NotificationType, timeout: number = MESSAGE_TIMEOUT) => {
        let timer: NodeJS.Timeout;

        if (timeout) {
            timer = setTimeout(() => {
                deleteNotification(nextId);
            }, timeout);
        }

        setNotifications((previousNotifications) => [...previousNotifications, { id: nextId, message, type, timer }]);
        setNextId(nextId + 1);
    };

    return (
        <NotificationsContext.Provider value={{ addNotification }}>
            {children}
            {!!notifications.length && (
                <div style={{ position: "fixed", bottom: 20, right: 20, zIndex: 9999 }}>
                    {notifications.map((notification) => (
                        <div key={notification.id} style={{ marginBottom: 8 }}>
                            <div
                                style={{
                                    padding: "8px 16px",
                                    borderRadius: 4,
                                    backgroundColor: notification.type === NotificationType.ERROR ? "red" : "green",
                                    color: "white",
                                    boxShadow: "0px 0px 4px rgba(0, 0, 0, 0.3)",
                                }}
                            >
                                {notification.message}
                                <button
                                    style={{
                                        marginLeft: 8,
                                        border: "none",
                                        backgroundColor: "transparent",
                                        cursor: "pointer",
                                        color: "white",
                                    }}
                                    onClick={() => deleteNotification(notification.id)}
                                >
                                    Dismiss
                                </button>
                            </div>
                        </div>
                    ))}
                </div>
            )}
        </NotificationsContext.Provider>
    );
};

export const useNotifications = () => {
    return React.useContext(NotificationsContext);
};
