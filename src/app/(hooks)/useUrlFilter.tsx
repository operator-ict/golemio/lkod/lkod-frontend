"use client";
import { useSearchParams } from "next/navigation";
import { useMemo } from "react";

import { genericFilter, IFilter } from "@/types/FilterInterface";
import { stringOrArr } from "@/utils/helpers";

export const useUrlFilter = () => {
    const searchParams = useSearchParams();

    const urlFilter = useMemo<genericFilter & IFilter>(
        () => ({
            publisherIri: searchParams?.get("publisherIri") || "",
            themeIris: stringOrArr(searchParams?.getAll("themeIris") || []),
            keywords: stringOrArr(searchParams?.getAll("keywords") || []),
            formatIris: stringOrArr(searchParams?.getAll("formatIris") || []),
            status: stringOrArr(searchParams?.getAll("status") || []),
        }),
        [searchParams]
    );

    const searchString = searchParams?.get("search") || "";

    return { urlFilter, searchString };
};
