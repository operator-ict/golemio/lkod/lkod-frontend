"use client";

import { useEffect, useState } from "react";
import { getFilesStatus } from "@/services/status-service";

export const useFilesStatus = () => {
    const [areFilesAvailable, setFilesAvailable] = useState(false);

    useEffect(() => {
        getFilesStatus()
            .then((response) => {
                setFilesAvailable(response.data === "OK" && response.status === 200);
            })
            .catch(() => setFilesAvailable(false));
    }, []);

    return { areFilesAvailable };
};
