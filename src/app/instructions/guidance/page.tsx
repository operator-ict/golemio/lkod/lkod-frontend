"use client";

import DatasetsImage from "public/assets/images/instruction/datasets.png";
import React, { FC } from "react";

import FileEdit from "/public/assets/images/instruction/datasetsEdit.png";
import LKODForm from "/public/assets/images/instruction/distr-link.png";
import CopyURL from "/public/assets/images/instruction/downloadfile.png";
import FormDataImage from "/public/assets/images/instruction/form.webp";
import FormDataStep2 from "/public/assets/images/instruction/form-2.png";
import PublicationDataset from "/public/assets/images/instruction/publicDataset.png";
import { Heading } from "@/components/Heading";
import text from "@/textContent/cs.json";

import DistributionSection from "../(DistributionSection )";
import { InstructionImage } from "../(ImageFrame)/InstructionImage";
import styles from "../Instructions.module.scss";

const Instructions: FC = () => {
    const createDatasetsInstructionParts = text.instructionsPage.createDatasetsInstruction.split("Přidat datovou sadu");

    return (
        <div className={styles.main}>
            <div className={styles["instruction-container"]}>
                <div className={styles["instruction-lkod"]}>
                    <Heading tag={`h2`} type={`h2`}>
                        {text.instructionsPage.datasets}
                    </Heading>
                    <Heading tag={`h3`} type={`h4`}>
                        {text.instructionsPage.createDatasets}
                    </Heading>
                    <span>
                        {createDatasetsInstructionParts[0]} <strong>Přidat datovou sadu</strong>
                        {createDatasetsInstructionParts[1]}
                    </span>
                    <InstructionImage src={DatasetsImage} alt="Dataset Creation Example" size="small" clickable />
                    <div className={styles["instruction-lkod__text-content"]}>
                        <span>{text.instructionsPage.createDatasetsInstructionNkod}</span>
                        <span>{text.instructionsPage.fillingDatasetsInstruction}</span>
                    </div>
                    <InstructionImage src={FormDataImage} alt="Form Data Example" size="large" />
                    <div className={styles["instruction-lkod__text-content"]}>
                        <span>{text.instructionsPage.fillingMetadataInstruction}</span>
                    </div>
                    <InstructionImage src={FormDataStep2} alt="Form Data Step 2 Example" size="large" />
                    <span>
                        {text.instructionsPage.distributionInstr.split("zelené")[0]}
                        <strong>{`zelené`}</strong>
                        {text.instructionsPage.distributionInstr.split("zelené")[1]}
                    </span>
                    <DistributionSection />
                    <InstructionImage src={FileEdit} alt="File Edit Example" size="large" clickable />
                    <span>{text.instructionsPage["copyThelinkURL:"]}</span>
                    <InstructionImage src={CopyURL} alt="Copy URL Example" size="large" clickable />
                    <span>{text.instructionsPage.insertinglinkintotheform}</span>
                    <InstructionImage src={LKODForm} alt="LKOD Form Example" size="large" />
                    <Heading tag={`h3`} type={`h4`}>
                        {text.instructionsPage.publicationDatasetInNKOD}
                    </Heading>
                    <span>{text.instructionsPage.publicationDatasetInNKODIstruction}</span>
                    <InstructionImage src={PublicationDataset} alt="Publication Dataset Example" size="small" />
                    <span>{text.instructionsPage.registersThedata}</span>
                    <Heading tag={`h3`} type={`h4`}>
                        {text.instructionsPage.editDataset}
                    </Heading>
                    <span>
                        {text.instructionsPage.editDatasetIstruction.split("Upravit")[0]}
                        <strong>Upravit</strong>
                        {text.instructionsPage.editDatasetIstruction.split("Upravit")[1]}
                    </span>
                    <span>
                        {text.instructionsPage.editDatasetIstructionDistr.split("Soubory")[0]}
                        <strong>Soubory</strong>
                        {text.instructionsPage.editDatasetIstructionDistr.split("Soubory")[1]}
                    </span>
                    <span>
                        {text.instructionsPage.editDatasetIstructionHide.split("Skrýt")[0]}
                        <strong>Skrýt</strong>
                        {text.instructionsPage.editDatasetIstructionHide.split("Skrýt")[1].split("Zveřejnit")[0]}
                        <strong>Zveřejnit</strong>
                        {text.instructionsPage.editDatasetIstructionHide.split("Zveřejnit")[1]}
                    </span>
                </div>
            </div>
        </div>
    );
};

export default Instructions;
