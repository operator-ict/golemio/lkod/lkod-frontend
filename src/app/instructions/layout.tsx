import "@/styles/reset.css";
import "@/styles/globals.scss";

import { FC, ReactNode } from "react";
import React from "react";

import text from "@/textContent/cs.json";

import { Heading } from "../(components)/Heading";
import Navigation from "./(Navigation)";
import styles from "./Instructions.module.scss";

interface InstructionLayoutProps {
    children: ReactNode;
}

const InstructionLayout: FC<InstructionLayoutProps> = async ({ children }) => {
    return (
        <main className={styles.main}>
            <div className={styles["top-container"]}>
                <Heading tag={`h1`}>{text.instructionsPage.title}</Heading>
                <span>{text.instructionsPage.subTitle}</span>
                <Navigation />
            </div>
            {children}
        </main>
    );
};

export default InstructionLayout;
