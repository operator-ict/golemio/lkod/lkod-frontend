import text from "@/textContent/cs.json";

import { Heading } from "../../(components)/Heading";
import styles from "./DistributionSection.module.scss";

const DistributionSection = () => {
    return (
        <section className={styles.distribution}>
            <Heading tag={`h3`} type={`h4`}>
                {text.instructionsPage.embeddingDistribution}
            </Heading>
            <div className={styles.item}>
                <span className={styles.number}>1)</span>
                <strong>{text.instructionsPage.embeddingDistributionTilte_1}</strong>
                <p>{text.instructionsPage.embeddingDistributionsubTilte_1}</p>
            </div>
            <div className={styles.item}>
                <span className={styles.number}>2)</span>
                <strong>{text.instructionsPage.embeddingDistributionTilte_2}</strong>
                <p>{text.instructionsPage.embeddingDistributionsubTilte_2}</p>
            </div>
        </section>
    );
};

export default DistributionSection;
