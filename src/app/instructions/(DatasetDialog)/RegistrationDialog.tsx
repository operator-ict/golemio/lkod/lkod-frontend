"use client";
import React, { FC, useCallback, useMemo, useState } from "react";

import Button from "@/components/Button";
import { ButtonScroll } from "@/components/ButtonScroll";
import { Card } from "@/components/Card";
import { Heading } from "@/components/Heading";
import HorizontalLine from "@/components/HorizontalLine";
import { Modal } from "@/components/Modal";
import text from "@/textContent/cs.json";

import { FilePathToClipboardDialog } from "../../(components)/FilePathToClipboardDialog";
import { LinkIcon } from "../../(components)/icons/LinkIcon";
import OrganizationLogo from "../../(components)/OrganizationLogo/OrganizationLogo";
import { Organization } from "../../(services)/organizations-service";
import styles from "./newDatasetDialog.module.scss";

export const RegistrationDialog: FC<{ isOpen: boolean; closeDialog: () => void; organizations: Organization[] }> = ({
    isOpen,
    closeDialog,
    organizations,
}) => {
    const LIST_LIMIT = 3;
    const [listStart, setListStart] = useState<number>(0);
    const [listEnd, setListEnd] = useState<number>(LIST_LIMIT);
    const [orgPath, setOrgPath] = useState<string | null>(null);

    const generatePath = useCallback((slug: string) => `https://api.opendata.praha.eu/lod/catalog?publishers%5B%5D=${slug}`, []);

    const scrollHandler = (direction: "left" | "right") => {
        const nextStart =
            direction === "right"
                ? Math.min(listStart + LIST_LIMIT, organizations.length - LIST_LIMIT)
                : Math.max(listStart - LIST_LIMIT, 0);

        const nextEnd = Math.min(nextStart + LIST_LIMIT, organizations.length);

        setListStart(nextStart);
        setListEnd(nextEnd);
    };

    const handlePath = (path: string) => {
        setOrgPath(orgPath === path ? null : path);
    };

    const visibleOrganizations = useMemo(() => organizations.slice(listStart, listEnd), [organizations, listStart, listEnd]);

    return (
        <Modal
            show={isOpen}
            onClose={closeDialog}
            label={text.chooseOrganization}
            headerText={text.instructionsPage.registration.ragistrdNKOD}
            /* eslint-disable max-len */
            headerWarning={`Pro generování odkazu pro registraci v NKOD vyberte požadovanou organizaci a klikněte na tlačítko "Získat odkaz".`}
        >
            <HorizontalLine />
            {organizations?.length === 0 && (
                <Heading tag="h6" color="secondary">
                    {text.errors.noOrganization}
                </Heading>
            )}
            {organizations?.length > 1 && (
                <div className={styles["organizations-navigation"]}>
                    {listStart > 0 && (
                        <ButtonScroll
                            color="secondary"
                            direction="left"
                            ariaLabel="scroll left"
                            onClick={() => scrollHandler("left")}
                        />
                    )}
                    {listEnd < organizations.length && (
                        <ButtonScroll
                            color="secondary"
                            direction="right"
                            ariaLabel="scroll right"
                            onClick={() => scrollHandler("right")}
                        />
                    )}
                </div>
            )}
            {organizations?.length > 0 && (
                <div className={styles["dialog-body"]}>
                    <div className={styles["organizations-container"]}>
                        {visibleOrganizations.map((organization) => (
                            <Card key={organization.id} className={styles["organization-card"]}>
                                <div className={styles["organization-card__heading"]}>
                                    <OrganizationLogo url={organization.logo} label="" size="small" shape="circle" />
                                    <Heading tag="h5">{organization.name}</Heading>
                                </div>

                                <div className={styles["file-card__btns"]}>
                                    <FilePathToClipboardDialog
                                        id={organization.id.toString()}
                                        isOpen={orgPath === organization.id.toString()}
                                        closePopup={() => setOrgPath(null)}
                                        filePath={generatePath(organization.slug)}
                                    >
                                        <Button
                                            color="outline-gray"
                                            onClick={() => handlePath(organization.id.toString())}
                                            label="Získat odkaz"
                                        >
                                            <LinkIcon color="gray" style={{ maxWidth: "1rem" }} />
                                        </Button>
                                    </FilePathToClipboardDialog>
                                </div>
                            </Card>
                        ))}
                    </div>
                    <Button color="outline-gray" onClick={closeDialog}>
                        {text.close}
                    </Button>
                </div>
            )}
        </Modal>
    );
};
