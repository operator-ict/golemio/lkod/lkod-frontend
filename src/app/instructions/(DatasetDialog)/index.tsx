"use client";
import { FC, useState } from "react";

import Button from "@/components/Button";
import text from "@/textContent/cs.json";

import { InfoIcon } from "../../(components)/icons/InfoIcon";
import styles from "../../(home)/Home.module.scss";
import { Organization } from "../../(services)/organizations-service";
import { RegistrationDialog } from "./RegistrationDialog";

interface IsFromArcGisProps {
    organizations: Organization[] | null;
}
const DatasetDialog: FC<IsFromArcGisProps> = ({ organizations }) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [addNewOpen, setAddNewOpen] = useState(false);

    return (
        <div>
            <Button
                color={`secondary`}
                label={text.instructionsPage.registration.linkLkodApi}
                className={styles["add-dataset"]}
                onClick={() => {
                    setAddNewOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                }}
            >
                <InfoIcon color="tertiary" />
            </Button>
            <RegistrationDialog
                closeDialog={() => setAddNewOpen(false)}
                isOpen={addNewOpen}
                organizations={organizations as Organization[]}
            />
        </div>
    );
};

export default DatasetDialog;
