import Link from "next/link";
import React, { FC } from "react";

import text from "@/textContent/cs.json";

import Catalog from "../../../../public/assets/images/instruction/catalog.png";
import CatalogStep2Image from "../../../../public/assets/images/instruction/catalog-step-2.png";
import { Heading } from "../../(components)/Heading";
import { ApiClient } from "../../(services)/initApi";
import DatsetDialog from "../(DatasetDialog)";
import { InstructionImage } from "../(ImageFrame)/InstructionImage";
import styles from "../Instructions.module.scss";

const Registration: FC = async () => {
    const { organizationsApi } = ApiClient;
    const organization = await organizationsApi.getAllOrganization();

    return (
        <section className={styles.main}>
            <div className={styles["instruction-container"]}>
                <div className={styles["instruction-lkod__text-content"]}>
                    <Heading tag={`h2`} type={`h2`}>
                        {text.instructionsPage.registration.registrationTitle}
                    </Heading>
                    <Heading tag={`h3`} type={`h4`}>
                        {text.instructionsPage.registration.registerOrganizationProfile}{" "}
                    </Heading>
                    <span>
                        {text.instructionsPage.registration.fillDigitalAgencyForm}{" "}
                        <Link
                            className={styles["instruction-lkod__link"]}
                            href={text.instructionsPage.registration.fillDigitalAgencyForm_link}
                        >
                            {text.instructionsPage.registration.fillDigitalAgencyForm_link}
                        </Link>
                    </span>
                    <ul className={styles["instruction-lkod__ul"]}>
                        <li>{text.instructionsPage.registration.catalogsName}</li>
                        <li>{text.instructionsPage.registration.nameEmail}</li>
                        <li>{text.instructionsPage.registration.catalogsType}</li>
                        <li>{text.instructionsPage.registration.lkodApiURL}</li>
                    </ul>
                </div>

                <DatsetDialog organizations={organization.data} />
                <div className={styles["instruction-container__image-block"]}>
                    <Heading tag={`h3`} type={`h4`}>
                        {text.instructionsPage.registration.sampleExample}
                    </Heading>

                    <InstructionImage src={Catalog} alt={"instuction image form step 1"} size={"large"} />
                    <span>
                        {text.instructionsPage.registration.onceCompleted.split("Shrnutí")[0]}
                        <strong>Shrnutí</strong>
                    </span>

                    <InstructionImage src={CatalogStep2Image} alt={"instuction image form step 2"} size={"large"} />
                    <div className={styles["instruction-lkod__text-content"]}>
                        <span>{text.instructionsPage.registration.downloadtheRegistrationFile}</span>
                        <span>{text.instructionsPage.registration.changeWillTakeEffect}</span>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Registration;
