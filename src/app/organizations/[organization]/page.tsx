import { notFound } from "next/navigation";
import React from "react";
import OrganizationForm from "src/app/organizations/(components)/OrganizationForm";

import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import OrgIconFrame from "@/components/OrganizationIconFrame";
import Loading from "@/loading";
import OrganizationActions from "@/organizations/[organization]/(components)/OrganizationActions/OrganizationActions";
import OrganizationMembers from "@/organizations/[organization]/(components)/OrganizationMembers";
import { ApiClient } from "@/services/initApi";

import styles from "./Organization.module.scss";

type params = {
    params: { organization: string };
};

const Organization = async ({ params }: params) => {
    const { organizationsApi, usersApi } = ApiClient;
    const organizationsResponse = await organizationsApi.getAllOrganization();
    const organizations = organizationsResponse.data ?? [];
    const urlOrganization: string = params.organization;

    const org = organizations.find((org) => org.slug === urlOrganization);

    if (!org) {
        notFound();
    }

    const members = (await organizationsApi.getOrganizationMembers(org.id ?? -1)).data ?? [];
    const users = (await usersApi.getAllUsers()).data ?? [];
    const nonMembers = users
        .filter((u) => !members.some((uu) => uu.id === u.id))
        .sort((a, b) => a.name.localeCompare(b.name) || a.email.localeCompare(b.email));

    const PAGE_LABEL = org?.name;

    return (
        <Container>
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <div className={styles["organization-container"]}>
                {org ? (
                    <OrgIconFrame src={org.logo ?? null} alt={org.name} name={org.name} description={org.description} />
                ) : (
                    <Loading />
                )}

                <div className={styles["organization-form-container"]}>
                    <Heading tag={`h1`} type={`h3`} className={`${styles["login-title"]}`}>
                        Informace
                    </Heading>
                    <OrganizationForm toUpdate={org} />
                </div>

                <div className={styles["organization-members-container"]}>
                    <Heading tag={`h1`} type={`h3`} className={`${styles["login-title"]}`}>
                        Členové
                    </Heading>
                    <OrganizationMembers members={members} nonMembers={nonMembers} organization={org} />
                </div>

                <div className={styles["organization-manage-container"]}>
                    <Heading tag={`h1`} type={`h3`} className={`${styles["login-title"]}`}>
                        Akce
                    </Heading>
                    <OrganizationActions organization={org} />
                </div>
            </div>
        </Container>
    );
};

export default Organization;
