"use client";

import { useRouter } from "next/navigation";
import React, { useState } from "react";

import Button from "@/components/Button";
import DeleteDialog from "@/components/DeleteDialog/DeleteDialog";
import OrganizationIcon from "@/components/icons/OrganizationIcon";
import { TrashIcon } from "@/components/icons/TrashIcon";
import { NotificationType, useNotifications } from "@/hooks/useNotifications";
import { deleteOrganization } from "@/organizations/actions";
import { getLogger } from "@/root/app/(logging)/logger";
import { getErrorMessage } from "@/root/app/(utils)/getErrorMessage";
import { ResponseType } from "@/services/api-client";
import { Organization } from "@/services/organizations-service";
import text from "@/textContent/cs.json";

import styles from "./OrganizationActions.module.scss";

interface OrganizationActionsProps {
    organization: Organization;
}
const logger = getLogger("OrganizationActions");

const OrganizationActions = ({ organization }: OrganizationActionsProps) => {
    const router = useRouter();
    const { addNotification } = useNotifications();
    const [deleteOpen, setDeleteOpen] = useState(false);

    const localDeleteOrganization = async () => {
        try {
            const response = await deleteOrganization(organization.id);
            navigateOrThrow(response);
        } catch (error) {
            logger.error({
                component: "OrganizationActions",
                action: "localDeleteOrganization",
                status: error.status,
                message: getErrorMessage(error),
            });
            addNotification("Organizaci se nepodařilo smazat", NotificationType.ERROR);
        }
    };

    const navigateOrThrow = (response: ResponseType<void>) => {
        if (response.status == 204) {
            router.replace(`/organizations/`);
            router.refresh();
        } else {
            throw { status: response.status };
        }
    };
    return (
        <div className={styles["organization-actions"]}>
            <Button type="button" color={"outline-secondary"} label={text.delete} onClick={() => setDeleteOpen(true)}>
                <TrashIcon color="secondary" style={{ minWidth: "0.9rem" }} />
            </Button>
            <DeleteDialog
                isOpen={deleteOpen}
                name={organization.name}
                titleIcon={<OrganizationIcon color="primary" />}
                titleText={text.organization.delete.prompt}
                subtitleText={text.organization.delete.subprompt}
                deleteButtonText={text.organization.delete.action}
                onConfirm={() => localDeleteOrganization()}
                closeDialog={() => setDeleteOpen(false)}
            />
        </div>
    );
};

export default OrganizationActions;
