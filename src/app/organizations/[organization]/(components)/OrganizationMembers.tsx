"use client";

import { useRouter } from "next/navigation";
import React, { useState } from "react";

import Button from "@/components/Button";
import { PlusIcon } from "@/components/icons/PlusIcon";
import { NotificationType, useNotifications } from "@/hooks/useNotifications";
import UserList from "@/modules/users/UserList/UserList";
import AddMemberDialog from "@/organizations/[organization]/(components)/AddMemberDialog";
import { addOrganizationMember, removeOrganizationMember } from "@/organizations/actions";
import { getLogger } from "@/root/app/(logging)/logger";
import { ResponseType } from "@/services/api-client";
import { Organization } from "@/services/organizations-service";
import { User } from "@/services/users-service";

interface OrganizationMembersProps {
    organization: Organization;
    members: User[];
    nonMembers: User[];
}
const logger = getLogger("OrganizationMembers");

const OrganizationMembers = (props: OrganizationMembersProps) => {
    const { addNotification } = useNotifications();
    const router = useRouter();

    const [isOpen, setIsOpen] = useState(false);

    const refreshOrThrow = (response: ResponseType<void>) => {
        if (response.status === 204) {
            router.replace(`/organizations/${props.organization?.slug}`);
            router.refresh();
        } else {
            throw { status: response.status };
        }
    };

    const addMember = async (user: User) => {
        setIsOpen(false);
        try {
            const response = await addOrganizationMember(props.organization.id as number, user.id);
            refreshOrThrow(response);
        } catch (error) {
            logger.error({
                component: "OrganizationMembers",
                action: "addMember",
                message: `Failed to add member to organization ${error.message}`,
                status: error.status,
            });
            addNotification("Člena se nepodařilo přidat", NotificationType.ERROR);
        }
    };

    const removeMember = async (user: User) => {
        try {
            const response = await removeOrganizationMember(props.organization.id as number, user.id);
            refreshOrThrow(response);
        } catch (error) {
            logger.error({
                component: "OrganizationMembers",
                organizationId: props.organization.id,
                userId: user.id,
                message: `Failed to remove member to organization ${error.message}`,
            });
            addNotification("Člena se nepodařilo odebrat", NotificationType.ERROR);
        }
    };

    return (
        <>
            <UserList users={props.members} action={"remove"} onClick={removeMember} />
            <Button color={`secondary`} label={`Přidat člena`} onClick={() => setIsOpen(true)}>
                <PlusIcon color="tertiary" />
            </Button>
            <AddMemberDialog
                isOpen={isOpen}
                closeDialog={() => setIsOpen(false)}
                nonMembers={props.nonMembers}
                organization={props.organization}
                onAdd={addMember}
            />
        </>
    );
};

export default OrganizationMembers;
