"use client";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React, { FC } from "react";

import { Card } from "@/components/Card";
import Project from "@/project.custom.json";
import text from "@/textContent/cs.json";

import styles from "./OrganizationCard.module.scss";

type Props = { label: string; logoUrl: string; description: string | null; slug: string | null };

export const OrganizationCard: FC<Props> = ({ label, logoUrl, slug }) => {
    const router = useRouter();

    const pushOrgHandler = (slug: string) => {
        if (slug) {
            router.push(`/organizations/${slug}`);
        } else {
            router.push("/organizations");
        }
    };

    const handleEditClick = (e: React.MouseEvent) => {
        e.stopPropagation();
        if (slug) {
            router.push(`/organizations/${slug}/edit`);
        }
    };

    return (
        <Card tag="button" onClick={() => pushOrgHandler(slug ?? "")} label={label}>
            <span className={styles["organization-card"]}>
                <span
                    className={`${styles["organization-card__image-frame"]} ${
                        logoUrl ? "" : styles["organization-card__image-frame_dull"]
                    }`}
                >
                    <Image
                        src={logoUrl ? logoUrl : Project.helpers.errorNoImage}
                        alt={label}
                        fill
                        sizes="(max-width: 576px) 100vw, (max-width: 992px) 33vw, 25vw"
                    />
                </span>
                <span className={styles["organization-card__label"]}>{label}</span>
                <span className={styles["organization-card__link"]} onClick={handleEditClick}>
                    <span className={styles["edit-link"]}>{text.edit} →</span>
                </span>
            </span>
        </Card>
    );
};
