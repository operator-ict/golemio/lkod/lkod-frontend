import React from "react";

import { PageNavigation } from "@/components/PageNavigation";
import { ResultLine } from "@/components/ResultLine";
import { OrganizationCard } from "@/organizations/(components)/OrganizationCard";
import styles from "@/organizations/Organizations.module.scss";
import { OrganizationsProps } from "@/organizations/page";
import { ApiClient } from "@/services/initApi";
import { Organization } from "@/services/organizations-service";
import text from "@/textContent/cs.json";
import { paginate } from "@/utils/pagination";
import matchesQuery from "@/utils/search";

const ORG_PER_PAGE_COUNT = 10;

const OrganizationsResults = async ({ searchParams }: OrganizationsProps) => {
    const { page, perpage, search } = searchParams;
    const { organizationsApi } = ApiClient;

    const response = await organizationsApi.getAllOrganization();
    const organizations = response.data ?? [];

    const filtered = filter(organizations, search);
    const paginated = paginate(filtered, page, perpage, ORG_PER_PAGE_COUNT);
    return (
        <>
            <ResultLine
                result={filtered.length}
                plurals={text.organization.search.plurals}
                foundPlurals={text.organization.search.foundPlurals}
            />
            <div className={styles["organization-list"]}>
                {paginated.map((org) => (
                    <OrganizationCard
                        key={org.id}
                        label={org.name}
                        logoUrl={org.logo ?? ""}
                        description={org.description}
                        slug={org.slug}
                    />
                ))}
            </div>
            <div className={styles.navigation}>
                <PageNavigation defaultPerPage={ORG_PER_PAGE_COUNT} page={page} perpage={perpage} count={filtered.length} />
            </div>
        </>
    );
};

const filter = (organizations: Organization[], search: string | undefined) => {
    const query = search ?? "";
    return organizations?.filter(
        (org) => matchesQuery(org.name, query) || matchesQuery(org.slug, query) || matchesQuery(org.identificationNumber, query)
    );
};

export default OrganizationsResults;
