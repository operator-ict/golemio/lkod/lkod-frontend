import React, { Suspense } from "react";

import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { Spinner } from "@/components/Spinner";
import { SearchForm } from "@/modules/search/SearchForm";
import AddOrganizationButton from "@/organizations/(components)/AddOrganizationButton";
import OrganizationsResults from "@/organizations/(components)/OrganizationsResults";
import text from "@/textContent/cs.json";

import styles from "./Organizations.module.scss";

export type OrganizationsProps = {
    searchParams: {
        page: string;
        perpage: string;
        search: string;
    };
};

const Organizations = async ({ searchParams }: OrganizationsProps) => {
    return (
        <Container>
            <div className={styles["top-container"]}>
                <Heading tag={`h1`}>{text.organization.organizations}</Heading>
                <AddOrganizationButton />
            </div>
            <SearchForm label={text.orgSearchString.title} placeholder={text.orgSearchString.placeholderText} fullWidth />
            <div className={styles["organizations-container"]}>
                <Suspense fallback={<Spinner />}>
                    <OrganizationsResults searchParams={searchParams} />
                </Suspense>
            </div>
        </Container>
    );
};

export default Organizations;
