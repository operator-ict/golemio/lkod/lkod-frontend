import React from "react";

import { Card } from "@/components/Card";
import { Heading } from "@/components/Heading";
import { EyeSlashIcon } from "@/components/icons/Eye-slash";
import { ReaderIcon } from "@/components/icons/ReaderIcon";
import { WarningIcon } from "@/components/icons/WarningIcon";
import { XCircleIcon } from "@/components/icons/XCircleIcon";
import { Text } from "@/components/Text";
import { User } from "@/services/users-service";
import text from "@/textContent/cs.json";
import { ValidatedDataset } from "@/types/ValidatedDatasetInterface";
import { capitalizeFirstLetter } from "@/utils/helpers";

import styles from "./DatasetCard.module.scss";

interface DatasetCardProps extends ValidatedDataset {
    user?: User;
}

const DatasetCard = ({ createdAt, updatedAt, name, organization, theme, status, validationResult, user }: DatasetCardProps) => {
    const localCreatedAt = new Date(createdAt).toLocaleString("cs", { hour12: false });

    const isInvalid = validationResult && validationResult.status;

    const localUpdatedAt = updatedAt ? new Date(updatedAt).toLocaleString("cs", { hour12: false }) : "n/a";

    return (
        <Card className={styles["dataset-card"]}>
            <p className={styles["dataset-card__top-line"]}>{`${theme} - ${organization.name}`}</p>
            <div className={styles["dataset-card__heading"]}>
                {status === "unpublished" && <EyeSlashIcon color={`gray`} height={`1rem`} />}
                {isInvalid && validationResult.status === "hasWarnings" && <WarningIcon color={"warning"} />}
                {isInvalid && validationResult.status === "invalid" && <XCircleIcon color={"alert"} />}
                <Heading tag={`h4`} type={`h5`}>
                    {name}
                </Heading>
                <ReaderIcon color={`gray-light`} style={{ marginLeft: "auto", height: "1.75rem", minWidth: "1.75rem" }} />
            </div>
            <div className={styles["dataset-card__info"]}>
                <Text size="md" className={styles["dataset-card__description"]}>
                    {text.status}
                    <span>{capitalizeFirstLetter(text.statuses[status])}</span>
                </Text>
                <Text size="md" className={styles["dataset-card__description"]}>
                    Čas úpravy: <span>{localUpdatedAt}</span>
                </Text>
                <Text size="md" className={styles["dataset-card__description"]}>
                    Čas vytvoření:
                    <span>{localCreatedAt}</span>
                </Text>
                {user && (
                    <Text size="md" className={styles["dataset-card__description"]}>
                        Vlastník:
                        <span>{user?.name}</span>
                    </Text>
                )}
            </div>
        </Card>
    );
};

export default DatasetCard;
