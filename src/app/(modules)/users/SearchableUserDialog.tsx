import React, { useState } from "react";

import { Modal } from "@/components/Modal";
import { SearchForm } from "@/modules/search/SearchForm";
import UserList from "@/modules/users/UserList/UserList";
import { User } from "@/services/users-service";
import text from "@/textContent/cs.json";
import { userMatchesQuery } from "@/utils/search";

interface SearchableUserDialogProps {
    label: string;
    headerText: string;
    isOpen: boolean;
    closeDialog: () => void;
    users: User[];
    onSelected: (user: User) => void;
}

const SearchableUserDialog = (props: SearchableUserDialogProps) => {
    const [query, setQuery] = useState("");
    const filtered = props.users.filter((u) => {
        return userMatchesQuery(u, query);
    });
    const limited = filtered.slice(0, 4);
    return (
        <Modal show={props.isOpen} onClose={props.closeDialog} label={props.label} headerText={props.headerText}>
            <div>
                <SearchForm placeholder={text.userSearchString.placeholderText} fullWidth asYouType={setQuery} />
                <UserList users={limited} action={"select"} onClick={props.onSelected} />
            </div>
        </Modal>
    );
};

export default SearchableUserDialog;
