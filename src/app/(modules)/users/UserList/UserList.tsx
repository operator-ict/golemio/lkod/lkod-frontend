import React from "react";

import Button from "@/components/Button";
import { Chip } from "@/components/Chip";
import { ChevronIcon } from "@/components/icons/ChevronIcon";
import { EditIcon } from "@/components/icons/EditIcon";
import { TrashIcon } from "@/components/icons/TrashIcon";
import styles from "@/modules/users/UserList/UserList.module.scss";
import { User } from "@/services/users-service";
import text from "@/textContent/cs.json";
import getRoleLabel from "@/utils/roles";

export type UserListAction = "select" | "remove" | "edit";

interface UserListProps {
    users: User[];
    action: "select" | "remove" | "manage";
    onClick: (user: User, action: UserListAction) => void;
    currentUserId?: number;
}

const UserList = (props: UserListProps) => {
    return (
        <ul className={styles["user-list"]}>
            {props.users.map((user) => (
                <li className={styles["user-list-item"]} key={user.id}>
                    <span className={styles["user-list-item__name"]}>{user.name}</span>
                    <span className={styles["user-list-item__email"]}>{user.email}</span>
                    <span className={styles["user-list-item__permissions"]}>
                        <Chip label={getRoleLabel(user.role)} />
                    </span>
                    {props.action === "remove" ? (
                        <Button
                            className={styles["user-list-item__action"]}
                            type="button"
                            color={"outline-secondary"}
                            label={text.delete}
                            onClick={() => props.onClick(user, "remove")}
                            hideLabel
                        >
                            <TrashIcon color="secondary" style={{ minWidth: "0.9rem" }} />
                        </Button>
                    ) : props.action === "select" ? (
                        <Button
                            className={styles["user-list-item__action"]}
                            color="secondary"
                            onClick={() => props.onClick(user, "select")}
                            title={text.createDataset}
                        >
                            {`Vybrat`}
                            <ChevronIcon color={`tertiary`} direction={`right`} width={`1.5rem`} height={`1.1rem`} />
                        </Button>
                    ) : user.id !== props.currentUserId ? (
                        <>
                            <Button
                                className={styles["user-list-item__action"]}
                                type="button"
                                color={"primary-light-outline"}
                                label={text.edit}
                                onClick={() => props.onClick(user, "edit")}
                            >
                                <EditIcon color="primary" style={{ minWidth: "0.9rem" }} />
                            </Button>
                            <Button
                                className={styles["user-list-item__secondary_action"]}
                                type="button"
                                color={"outline-secondary"}
                                label={text.delete}
                                onClick={() => props.onClick(user, "remove")}
                                hideLabel
                            >
                                <TrashIcon color="secondary" style={{ minWidth: "0.9rem" }} />
                            </Button>
                        </>
                    ) : (
                        <></>
                    )}
                </li>
            ))}
        </ul>
    );
};

export default UserList;
