/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React, { MouseEvent, useState } from "react";

import { CheckBox } from "@/components/CheckBox";
import { Heading } from "@/components/Heading";
import { ListLimiter } from "@/components/ListLimiter";

type Props = {
    className: string;
    checkerCondition: string | string[];
    data: {
        iri?: string;
        label: string;
        count: string;
    }[];
    fnc: (val: string, key: string) => void;
    label: string;
    limit: number;
    section: string;
};

export const FilterFormSection = ({ className, checkerCondition, data, fnc, label, limit, section }: Props) => {
    const [listLimit, setListLimit] = useState<number>(limit);

    const limitHandler = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        if (listLimit + limit < data.length) {
            setListLimit((prev) => prev + limit);
        } else if (listLimit + limit >= data.length) setListLimit(data.length);
        if (listLimit >= data.length) {
            setListLimit(limit);
        }
    };

    return (
        <form className={className} tabIndex={0}>
            <fieldset id={section}>
                <legend>
                    <Heading tag={`h4`} type={`h5`}>
                        {label}
                    </Heading>
                    <span aria-label={data.length + "elementů"}>{data.length}</span>
                </legend>
                {data &&
                    data
                        .slice(0, listLimit)
                        .map((item) => (
                            <CheckBox
                                key={item.iri || item.label}
                                label={item.label}
                                count={item.count}
                                id={item.iri || item.label}
                                checked={
                                    checkerCondition
                                        ? typeof checkerCondition === "string"
                                            ? checkerCondition === item.iri
                                            : checkerCondition.includes(item.iri || item.label)
                                        : false
                                }
                                onClick={() => (!item.iri ? fnc(item.label, section) : fnc(item.iri, section))}
                            />
                        ))}
                {data.length > limit && <ListLimiter rest={data.length - listLimit} limit={limit} onClick={limitHandler} />}
            </fieldset>
        </form>
    );
};
