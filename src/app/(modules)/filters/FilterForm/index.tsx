"use client";
import { useRouter } from "next/navigation";
import React, { DetailedHTMLProps, FC, Suspense, use } from "react";

import Button from "@/components/Button";
import { ButtonReset } from "@/components/ButtonReset";
import {
    useFormatsPromise,
    useKeywordsPromise,
    usePublishersPromise,
    useStatusesPromise,
    useThemesPromise,
} from "@/context/DataProvider";
import { useUrlFilter } from "@/hooks/useUrlFilter";
import { Spinner } from "@/root/app/(components)/Spinner";
import text from "@/textContent/cs.json";
import { objectToQueryString } from "@/utils/objectToQueryString";

import { FilterFormSection } from "../FilterFormSection";
import { StatusFilterFormSection } from "../StatusFilterFormSection";
import styles from "./FilterForm.module.scss";

type Props = DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> & {
    onClose: () => void;
    publisherIri?: string;
};

export const FilterForm: FC<Props> = ({ onClose, className, publisherIri }) => {
    const router = useRouter();

    const { urlFilter } = useUrlFilter();

    const publishers = use(usePublishersPromise());
    const topics = use(useThemesPromise());
    const keywords = use(useKeywordsPromise());
    const formats = use(useFormatsPromise());
    const statuses = use(useStatusesPromise());

    const filterPublisherHandler = (val: string, key: string) => {
        if (urlFilter[key] === val) {
            urlFilter.publisherIri = "";
        } else {
            urlFilter.publisherIri = val;
        }

        const queryString = objectToQueryString(urlFilter);
        router.push(`?${queryString}`, { scroll: false });
    };

    const filterArrHandler = (val: string, key: string) => {
        if (urlFilter[key].includes(val)) {
            urlFilter[key] = (urlFilter[key] as string[]).filter((value) => value !== val);
        } else if (!urlFilter[key]) {
            urlFilter[key] = [val];
        } else if (urlFilter[key] && !urlFilter[key].includes(val)) {
            (urlFilter[key] as string[]).push(val);
        }
        const queryString = objectToQueryString(urlFilter);
        router.push(`?${queryString}`, { scroll: false });
    };

    if (publisherIri) {
        urlFilter.publisherIri = publisherIri;
    }

    const filterStatusHandler = (val: string | string[]) => {
        if (Array.isArray(val)) {
            urlFilter.status = val;
        } else {
            if (urlFilter.status.includes(val)) {
                urlFilter.status = urlFilter.status.filter((status) => status !== val);
            } else {
                urlFilter.status = [...urlFilter.status, val];
            }
        }
        const queryString = objectToQueryString(urlFilter);
        router.push(`?${queryString}`, { scroll: false });
    };
    return (
        <div className={className}>
            <div className={styles.wrapper}>
                <Suspense fallback={<Spinner />}>
                    <StatusFilterFormSection
                        className={styles["filter-section"]}
                        label={text.filtersSection.status}
                        data={statuses}
                        section={`status`}
                        fnc={filterStatusHandler}
                        checkerCondition={urlFilter.statuses as string[]}
                    />
                    {publishers && publishers.length > 0 && (
                        <FilterFormSection
                            className={styles["filter-section"]}
                            label={text.filtersSection.organization}
                            section={`publisherIri`}
                            data={publishers}
                            fnc={filterPublisherHandler}
                            checkerCondition={urlFilter.publisherIri as string}
                            limit={4}
                        />
                    )}
                    {topics && topics.length > 0 && (
                        <FilterFormSection
                            className={styles["filter-section"]}
                            label={text.filtersSection.theme}
                            section={`themeIris`}
                            data={topics}
                            fnc={filterArrHandler}
                            checkerCondition={urlFilter.themeIris as string[]}
                            limit={4}
                        />
                    )}
                    {keywords && keywords.length > 0 && (
                        <FilterFormSection
                            className={styles["filter-section"]}
                            label={text.filtersSection.keywords}
                            data={keywords}
                            section={`keywords`}
                            fnc={filterArrHandler}
                            checkerCondition={urlFilter.keywords as string[]}
                            limit={4}
                        />
                    )}
                    {formats && formats.length > 0 && (
                        <FilterFormSection
                            className={styles["filter-section"]}
                            data={formats}
                            label={text.filtersSection.formats}
                            section={`formatIris`}
                            fnc={filterArrHandler}
                            checkerCondition={urlFilter.formatIris as string[]}
                            limit={4}
                        />
                    )}
                </Suspense>
            </div>
            <section className={styles["filter-section__form-actions"]}>
                <Button
                    label={text.filters.confirmFilters}
                    className={styles["mobile-visible"]}
                    color="primary"
                    onClick={() => {
                        onClose();
                    }}
                />

                <ButtonReset
                    label={text.filters.cancelFilters}
                    onClick={() => {
                        router.push(`?`);
                    }}
                />
            </section>
        </div>
    );
};
