/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React, { FC } from "react";

import { CheckBox } from "@/components/CheckBox";
import { Heading } from "@/components/Heading";
import text from "@/textContent/cs.json";
import { IStatus } from "@/types/LookupTypes";
import { capitalizeFirstLetter } from "@/utils/helpers";

type Props = {
    className: string;
    checkerCondition: string[];
    data: IStatus[];
    fnc: (val: string, key: string) => void;
    label: string;
    section: string;
};

export const StatusFilterFormSection: FC<Props> = ({ className, checkerCondition = [], data, fnc, label, section }) => {
    return (
        <form className={className}>
            <fieldset id={section}>
                <legend>
                    <Heading tag={`h4`} type={`h5`}>
                        {label}
                    </Heading>
                </legend>
                {data.map((status) => {
                    return (
                        <CheckBox
                            key={status.label}
                            label={capitalizeFirstLetter(text.statuses[status.label as keyof typeof text.statuses])}
                            count={`${status.count}`}
                            id={status.label}
                            checked={checkerCondition.includes(status.label)}
                            onClick={() => fnc(status.label, label)}
                        />
                    );
                })}
            </fieldset>
        </form>
    );
};
