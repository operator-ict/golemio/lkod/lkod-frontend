"use client";
import { useRouter } from "next/navigation";
import { useEffect } from "react";

import { CardInfo } from "@/components/CardInfo";
import ColorLink from "@/components/ColorLink";
import { Heading } from "@/components/Heading";
import { NoResultIcon } from "@/components/icons/NoResultIcon";

import classes from "./FiveHundred.module.scss";

// eslint-disable-next-line react/function-component-definition
export default function Error({ error }: { error: Error & { digest?: string }; reset: () => void }) {
    const router = useRouter();
    useEffect(() => {
        console.error(error);
    }, [error]);
    return (
        <main className={classes.main}>
            <div className={classes.container}>
                <CardInfo className={classes["five-hundred"]}>
                    <NoResultIcon />
                    <Heading tag={`h1`} type={`h3`}>
                        500 - Bohužel, chyba na serveru.
                    </Heading>
                    <ColorLink linkText="Zpět" onClick={() => router.back()} direction="left" center />
                </CardInfo>
            </div>
        </main>
    );
}
