import { getLogger } from "@/logging/logger";

import { getErrorMessage } from "../(utils)/getErrorMessage";

const logger = getLogger("checkTokenExpiration");

export async function validateSessionToken(token: string, role: string) {
    try {
        const roles = {
            isAdmin: role === "admin",
            isSuperAdmin: role === "superadmin",
            isUser: role === "user",
        };

        const base64Payload = token.split(".")[1];
        if (!base64Payload) {
            throw new Error("Invalid token format");
        }

        const decodedPayload = JSON.parse(atob(base64Payload));
        const expiryTimestamp = decodedPayload.exp;

        if (typeof expiryTimestamp !== "number") {
            throw new Error("Invalid or missing expiry timestamp in token");
        }

        const isExpired = expiryTimestamp < Date.now() / 1000;

        return { ...roles, isExpired };
    } catch (error) {
        logger.error({
            provider: "server",
            component: "validateSessionToken",
            message: getErrorMessage(error),
        });
        throw error;
    }
}
