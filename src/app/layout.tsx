import "@/styles/reset.css";
import "@/styles/globals.scss";

import { Session } from "next-auth";
import { getServerSession } from "next-auth/next";
import { FC, ReactNode } from "react";
import React from "react";
import { config } from "src/config";

import Footer from "@/components/Footer";
import Header from "@/components/Header";
import { roboto } from "@/fonts/fonts";

import { NotificationsProvider } from "./(hooks)/useNotifications";
import { DatasetsProvider } from "./(state)/datasetsState";
import { OrganizationsProvider } from "./(state)/organizationsState";
import { authOptions } from "./api/auth/[...nextauth]/authOptions";
import styles from "./Layout.module.scss";

export const dynamic = "force-dynamic";

interface RootLayoutProps {
    children: ReactNode;
}

const CatalogUrl = config.CATALOG_URL;
const ShowCatalogLink = config.SHOW_CATALOG_LINK;
const ExternalFormUrl = config.EXTERNAL_FORM_URL;
const returnUrl = config.RETURN_URL;

const ProvidersContext: FC<{ children: ReactNode; session: Session | null }> = async ({ children, session }) => {
    return (
        <DatasetsProvider externalUrl={ExternalFormUrl} returnUrl={returnUrl} session={session}>
            <OrganizationsProvider>
                <NotificationsProvider>{children}</NotificationsProvider>
            </OrganizationsProvider>
        </DatasetsProvider>
    );
};
const RootLayout: FC<RootLayoutProps> = async ({ children }) => {
    const session = await getServerSession(authOptions);

    return (
        <html lang="cs" className={roboto.variable}>
            <head>
                <link rel="canonical" href={`${config.NEXT_PUBLIC_URL}`} />
            </head>
            <body className={styles["main-layout"]}>
                <ProvidersContext session={session}>
                    <Header session={session} />
                    <div id="modal-root" />
                    {children}
                    <Footer url={CatalogUrl} showCatalogLink={ShowCatalogLink} session={session} />
                </ProvidersContext>
            </body>
        </html>
    );
};

export default RootLayout;
