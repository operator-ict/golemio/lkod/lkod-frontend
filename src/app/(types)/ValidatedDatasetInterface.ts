import { Dataset } from "@/types/Dataset";

export interface IValidationResult {
    status: "valid" | "invalid" | "hasWarnings";
    messages: string[] | null;
}

export interface ValidatedDataset extends Dataset {
    validationResult: IValidationResult;
}

export interface IDatasetValidationData {
    result: IValidationResult;
    label?: string | null;
}
