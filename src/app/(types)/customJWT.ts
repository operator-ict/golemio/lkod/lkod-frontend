export interface JWTLkod {
    id: number;
    admin: boolean;
    iat: number;
    exp: number;
    iss: string;
    jti: string;
}
