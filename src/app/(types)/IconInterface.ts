export interface IiCon {
    color: "primary" | "primary-light" | "secondary" | "tertiary" | "black" | "gray" | "gray-light" | "alert" | "warning";
    direction?: "up" | "down" | "left" | "right";
}
