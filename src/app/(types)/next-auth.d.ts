/* eslint-disable @typescript-eslint/no-unused-vars */
import { Session } from "next-auth";
import { JWT } from "next-auth/jwt";

import { UserRole } from "@/services/authorization-service";

/** Example on how to extend the built-in session types */
declare module "next-auth" {
    interface Session {
        /** This is an example. You can find me in types/next-auth.d.ts */
        token: string;
        defaultPassword: boolean | unknown;
        isLoggedIn: boolean;
        user: { email: string; role: UserRole };
    }
    interface User {
        defaultPassword: boolean;
        role: UserRole;
        token: string;
    }
}

/** Example on how to extend the built-in types for JWT */
declare module "next-auth/jwt" {
    interface JWT {
        /** This is an example. You can find me in types/next-auth.d.ts */
        token: string;
    }
}
