import { Organization } from "@/services/organizations-service";

import { IFilter } from "./FilterInterface";

export const DatasetStatus = {
    Created: "created",
    Saved: "saved",
    Published: "published",
    Unpublished: "unpublished",
} as const;

export type IDatasetStatus = (typeof DatasetStatus)[keyof typeof DatasetStatus];

export const GetAllDatasetsStatus = {
    Created: "created",
    Saved: "saved",
    Published: "published",
    Unpublished: "unpublished",
} as const;

export type GetAllDatasetsStatus = (typeof GetAllDatasetsStatus)[keyof typeof GetAllDatasetsStatus];

export interface UpdateDatasetInput {
    status: UpdateDatasetInputStatus;
    userId?: number;
}

export const UpdateDatasetInputStatus = {
    Published: "published",
    Unpublished: "unpublished",
} as const;

export type UpdateDatasetInputStatus = (typeof UpdateDatasetInputStatus)[keyof typeof UpdateDatasetInputStatus];

export interface CreateDatasetInput {
    organizationId: number;
}

export interface GetAllFilesRequest {
    datasetId: string;
}

export interface UploadedFile {
    files: string[];
}

export interface Consume {
    contentType: string;
}

export interface UploadFileRequest {
    datasetId: string;
    datasetFile: File;
    //datasetFile: FormData;
}

export interface Dataset {
    id: string;
    isReadOnly: boolean;
    organizationId: number;
    userId: number;
    status: IDatasetStatus;
    createdAt: Date;
    updatedAt?: Date | null;
    organization: Organization;
    data?: { [key: string]: unknown } | null;
    name?: string | null;
    theme?: string;
}

export interface CreateDatasetRequest {
    createDatasetInput: CreateDatasetInput;
}

export interface DatasetId {
    datasetId: string;
}

export interface DeleteFileRequest {
    datasetId: string;
    filename: string;
}

export interface DatasetsFiltered {
    isReadOnly: boolean;
    id: string;
    organizationId: number;
    userId: number;
    status: string;
    createdAt: string;
    updatedAt: string;
    data: DatasetInformation;
    validationResult: validationResult;
    name: string;
    keywords: string[];
    organization: OrganizationFilter;
}
interface OrganizationFilter {
    id: number;
    name: string;
    identificationNumber: string;
    slug: string;
    logo: string | null;
    description: string | null;
    arcGisFeed: string | null;
}
interface validationResult {
    status: string;
    messages: string[];
}
interface DatasetInformation {
    iri: string;
    typ: string;
    popis: object;
    téma: string[];
    název: object;
    "@context": string;
    distribuce: string[];
    poskytovatel: string;
    prvek_rúian: string[];
    kontaktní_bod: object;
    koncept_euroVoc: string[];
    klíčové_slovo: object;
    geografické_území: string[];
    prostorové_pokrytí: string[];
    periodicita_aktualizace: string;
}

export interface CreateSession {
    createdAt?: string | Date | undefined;
    datasetId: string;
    expiresIn: number;
    id: string;
    userId: number;
}

export interface DatasetFilters {
    limit: number;
    offset: number;
    filter: IFilter;
    searchString: string;
}
