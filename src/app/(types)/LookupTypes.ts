export type IStatus = {
    label: string;
    count: string;
};

export type IPublisher = {
    iri: string;
    label: string;
    count: string;
};
export type IThemes = {
    iri: string;
    label: string;
    count: string;
};

export type IFormats = {
    iri: string;
    label: string;
    count: string;
};

export type IKeywords = {
    label: string;
    count: string;
};
