export interface IFilter {
    status: string[];
    publisherIri: string;
    themeIris: string[];
    keywords: string[];
    formatIris: string[];
}

export type genericFilter = {
    [key: string]: string[] | string;
};
