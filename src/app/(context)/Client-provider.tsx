"use client";
import { Session } from "next-auth";
import { SessionProvider } from "next-auth/react";
import { ReactNode } from "react";

interface IProviderProps {
    children: ReactNode;
    session: Session | null;
}
export const SessionAuthProvider = ({ children, session }: IProviderProps) => {
    return <SessionProvider session={session}>{children}</SessionProvider>;
};
