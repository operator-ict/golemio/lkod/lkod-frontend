import React, { ReactNode, useEffect } from "react";

import { useComponentVisible } from "@/hooks/useComponentVisible";

import styles from "./Popup.module.scss";

interface IPopup {
    visible: boolean;
    hide: () => void;
    content?: ReactNode;
    children: ReactNode;
}

export const Popup = ({ content, children, visible, hide, ...restProps }: IPopup) => {
    const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(visible, () => hide());

    useEffect(() => {
        setIsComponentVisible(visible);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visible]);

    return (
        <div className={styles["popup_wrapper"]}>
            {isComponentVisible && (
                <div ref={ref} className={styles["popup"]} {...restProps}>
                    {content}
                </div>
            )}
            <div>{children}</div>
        </div>
    );
};
