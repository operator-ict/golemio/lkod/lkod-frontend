"use client";
import React, { FC, useContext, useState } from "react";

import Button from "@/components/Button";
import { ButtonScroll } from "@/components/ButtonScroll";
import { Card } from "@/components/Card";
import { Heading } from "@/components/Heading";
import HorizontalLine from "@/components/HorizontalLine";
import { ChevronIcon } from "@/components/icons/ChevronIcon";
import OrganizationIcon from "@/components/icons/OrganizationIcon";
import { Modal } from "@/components/Modal";
import text from "@/textContent/cs.json";

import { Organization } from "../../(services)/organizations-service";
import { DatasetsContext } from "../../(state)/datasetsState";
import styles from "./newDatasetDialog.module.scss";

export const NewDatasetDialog: FC<{ isOpen: boolean; closeDialog: () => void; organizations: Organization[] }> = ({
    isOpen,
    closeDialog,
    organizations,
}) => {
    const { createDataset, isCreateLoading, datasetsCreateError } = useContext(DatasetsContext);
    const LIST_LIMIT = 3;

    const [listStart, setListStart] = useState<number>(0);
    const [listEnd, setListEnd] = useState<number>(LIST_LIMIT);
    const scrollHandler = (direction: string) => {
        if (direction === "right") {
            if (listEnd < organizations.length - 1) {
                setListStart(listStart + LIST_LIMIT);
                setListEnd(listEnd + LIST_LIMIT);
            } else {
                setListStart(organizations.length - LIST_LIMIT);
                setListEnd(organizations.length);
            }
        }
        if (direction === "left") {
            if (listStart > 1) {
                setListStart(listStart - LIST_LIMIT);
                setListEnd(listEnd - LIST_LIMIT);
            } else {
                setListStart(0);
                setListEnd(LIST_LIMIT);
            }
        }
    };

    return (
        <Modal
            show={isOpen}
            onClose={closeDialog}
            label={text.chooseOrganization}
            headerText={text.addNewDataset}
            headerWarning={`Pro přidání datové sady budete přesměrováni na web ministerstva.`}
        >
            <HorizontalLine />
            <>
                {organizations && organizations.length === 0 && (
                    <Heading tag={`h6`} color="secondary">
                        {text.errors.noOrganization}
                    </Heading>
                )}
                {organizations?.length > 1 && (
                    <div className={styles["organizations-navigation"]}>
                        <ButtonScroll
                            color="secondary"
                            direction="left"
                            ariaLabel="direction left"
                            onClick={() => scrollHandler("left")}
                            disabled={listStart === 0}
                        />
                        <ButtonScroll
                            color="secondary"
                            direction="right"
                            ariaLabel="direction right"
                            onClick={() => scrollHandler("right")}
                            disabled={listEnd === organizations.length}
                        />
                    </div>
                )}
                {organizations?.length > 0 && (
                    <div className={styles["dialog-body"]}>
                        <div className={styles["organizations-container"]}>
                            {organizations.slice(listStart, listEnd).map((organization) => (
                                <Card key={organization.id} className={styles["organization-card"]}>
                                    <div className={styles["organization-card__heading"]}>
                                        <OrganizationIcon color={"gray-light"} style={{ minWidth: "1.5rem", height: "1.5rem" }} />
                                        <Heading tag={`h5`}>{organization.name}</Heading>
                                    </div>
                                    <Button
                                        color="secondary"
                                        disabled={isCreateLoading}
                                        onClick={() => {
                                            createDataset(organization.id);
                                        }}
                                        title={text.createDataset}
                                    >
                                        {`Vybrat`}
                                        <ChevronIcon color={`tertiary`} direction={`right`} width={`1.5rem`} height={`1.1rem`} />
                                    </Button>
                                </Card>
                            ))}
                        </div>
                        <Button color="outline-gray" onClick={closeDialog}>
                            {text.close}
                        </Button>
                        {!!datasetsCreateError && (
                            <Heading tag={`h6`} color={`secondary`}>
                                {datasetsCreateError}
                            </Heading>
                        )}
                    </div>
                )}
            </>
        </Modal>
    );
};
