/* eslint-disable react/display-name */
import Image, { ImageProps } from "next/image";
import Link from "next/link";
import React, { forwardRef } from "react";

import logo from "@/logos/logoOwner.svg";
import text from "@/textContent/cs.json";

import styles from "./Logo.module.scss";

type LogoProps = ImageProps & {
    footer?: boolean;
};

export const Logo = forwardRef<HTMLDivElement, LogoProps>(({ footer }, ref) => {
    return (
        <Link className={`${styles["frame"]}`} href={`/`}>
            <div className={`${styles["logo-frame"]} ${footer ? styles["logo-frame_footer"] : ""}`} ref={ref}>
                <Image src={logo} alt={`${text.configurable.logoOwner}`} width={96} height={96} />
            </div>
            <div className={`${styles.text} ${footer ? styles["text-footer"] : ""}`}>
                <p className={styles.link}>{text.configurable.logoText}</p>
            </div>
        </Link>
    );
});
