"use client";
import { Session } from "next-auth";
import React, { useEffect, useRef, useState } from "react";

import { Logo } from "@/components/configurable/Logo";
import { NavBar } from "@/components/NavBar";

import styles from "./Header.module.scss";

interface HeaderProps {
    session: Session | null;
}

const Header = ({ session }: HeaderProps) => {
    const [isShrunk, setShrunk] = useState(false);
    const logoRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const handler = () => {
            setShrunk((isShrunk) => {
                if (!isShrunk && (document.body.scrollTop > 55 || document.documentElement.scrollTop > 55)) {
                    return true;
                }

                if (isShrunk && document.body.scrollTop < 4 && document.documentElement.scrollTop < 4) {
                    return false;
                }

                return isShrunk;
            });
        };

        window.addEventListener("scroll", handler);
        return () => window.removeEventListener("scroll", handler);
    }, []);

    return (
        <header className={`${styles.header} ${isShrunk ? styles["header_scrolled"] : ""}`}>
            <Logo src={""} ref={logoRef} alt={"image logo"} />
            <NavBar session={session} />
        </header>
    );
};

export default Header;
