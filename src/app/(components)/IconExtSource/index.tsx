import Image from "next/image";
import React, { FC } from "react";

import styles from "./IconExtSource.module.scss";

type Props = {
    src: string;
    alt: string;
};

const IconExtSource: FC<Props> = ({ src, alt }) => {
    return (
        <div className={styles["icon-external"]}>
            <Image src={src} fill alt={alt} />
        </div>
    );
};

export default IconExtSource;
