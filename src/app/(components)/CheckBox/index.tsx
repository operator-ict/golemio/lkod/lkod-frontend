import React, { ChangeEvent, DetailedHTMLProps, InputHTMLAttributes, useEffect, useState } from "react";

import styles from "./CheckBox.module.scss";

interface InputElementProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    count?: string;
    label: string;
    onClick: () => void;
}

export const CheckBox = ({ checked, count, id, label, name, onClick }: InputElementProps) => {
    const defaultChecked = checked ? checked : false;
    const [isChecked, setIsChecked] = useState(defaultChecked);

    const changeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        setIsChecked(e.target.checked);
        onClick();
    };

    useEffect(() => {
        if (!checked) {
            setIsChecked(false);
        }
    }, [checked]);

    return (
        <div className={styles["checkbox-wrapper"]}>
            <label htmlFor={id}>
                <input type="checkbox" id={id} name={name} checked={isChecked} onChange={changeHandler} />
                <span>{label}</span>
                <span className={styles.count} aria-label={count}>
                    {count}
                </span>
            </label>
        </div>
    );
};
