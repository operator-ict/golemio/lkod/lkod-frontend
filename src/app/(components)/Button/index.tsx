/* eslint-disable react/display-name */
import React, { ButtonHTMLAttributes, DetailedHTMLProps, forwardRef, Ref } from "react";

import style from "./Button.module.scss";

type Colors =
    | "primary"
    | "primary-light"
    | "primary-light-outline"
    | "primary-light-outline-info"
    | "secondary"
    | "outline-secondary"
    | "outline-gray"
    | "transparent"
    | "outline_alert"
    | "outline_warning";

interface ButtonProps extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    label?: string;
    color: Colors;
    icon?: string;
    hideLabelMobile?: boolean;
    hideLabel?: boolean;
}

const Button = forwardRef<HTMLButtonElement, ButtonProps>((componentProps: ButtonProps, ref: Ref<HTMLButtonElement>) => {
    const {
        className,
        color,
        children,
        label,
        onClick,
        type = "button",
        disabled,
        hideLabelMobile,
        hideLabel,
        id,
    } = componentProps;

    return (
        <button
            ref={ref}
            onClick={onClick}
            type={type}
            className={`${style["button"]} ${style[`button_color_${color}`]} ${disabled ? style.disabled : ""} ${className}`}
            disabled={disabled}
            aria-label={label}
            id={id}
        >
            {children}
            {!hideLabel && label && <span className={`${hideLabelMobile ? style.label : ""}`}>{label}</span>}
        </button>
    );
});

export default Button;
