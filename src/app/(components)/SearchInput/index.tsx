"use client";
import { ChangeEvent, DetailedHTMLProps, FC, InputHTMLAttributes } from "react";

import { SearchIcon } from "../icons/SearchIcon";
import styles from "./SearchInput.module.scss";

interface SearchInputProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    fullWidth?: boolean;
    resetInputField?: () => void;
    updateInputValue: (value: string) => void;
}

export const SearchInput: FC<SearchInputProps> = ({
    fullWidth,
    id,
    name,
    onKeyDown,
    placeholder,
    resetInputField,
    updateInputValue,
    value,
    ...restProps
}: SearchInputProps) => {
    const updateValue = (event: ChangeEvent) => {
        updateInputValue((event.target as HTMLInputElement).value);
    };

    return (
        <div className={styles["search-wrapper"]}>
            <div className={styles["icon-frame"]}>
                <SearchIcon />
            </div>
            <input
                onChange={updateValue}
                className={`${styles["search-input-field"]} ${fullWidth ? styles.full : ""}`}
                type="text"
                id={id}
                name={name}
                placeholder={placeholder}
                value={value}
                onKeyDown={onKeyDown}
                {...restProps}
            />
            {value && (
                <span className={`${styles["icon-frame"]} ${styles["icon-frame-right"]}`} onClick={resetInputField}>
                    &times;
                </span>
            )}
        </div>
    );
};
