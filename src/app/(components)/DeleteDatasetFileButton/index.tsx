import React, { FC, ReactNode, useState } from "react";

import { Heading } from "@/components/Heading";
import { TrashIcon } from "@/components/icons/TrashIcon";
import { Spinner } from "@/components/Spinner";
import { NotificationType } from "@/hooks/useNotifications";
import text from "@/textContent/cs.json";

import { dataServiceClient } from "../../(services)/client/datasets-service-client";
import Trans from "../Trans";
import styles from "./deleteDatasetFileButton.module.scss";

interface Props {
    datasetId: string;
    filename: string;
    onError: (message: ReactNode, type: NotificationType) => void;
}

enum FileState {
    READY,
    LOADING,
    NOT_FOUND,
    DELETED,
}

export const DeleteDatasetFileButton: FC<Props> = ({ datasetId, filename, onError }) => {
    const [state, setState] = useState<FileState>(FileState.READY);

    const onClick = () => {
        if (state === FileState.LOADING) {
            return;
        }

        setState(FileState.LOADING);
        dataServiceClient
            .deleteFile({ datasetId, filename })
            .then(() => setState(FileState.DELETED))
            .catch((err) => {
                if (err.status === 404) {
                    onError(
                        <Trans i18nKey="errors.fileNotFound" values={{ filename }} components={[<b key={1} />]} />,
                        NotificationType.ERROR
                    );
                    setState(FileState.NOT_FOUND);
                } else {
                    onError(
                        <Trans i18nKey="errors.unknownDeleteFileError" values={{ filename }} components={[<b key={2} />]} />,
                        NotificationType.ERROR
                    );
                    setState(FileState.READY);
                }
            });
    };

    if (state === FileState.DELETED) {
        return (
            <Heading tag={`h6`} color="secondary">
                {text.deleted}
            </Heading>
        );
    }

    if (state === FileState.NOT_FOUND) {
        return (
            <Heading tag={`h6`} color="secondary">
                {text.notFound}
            </Heading>
        );
    }
    return (
        <>
            {state === FileState.LOADING ? (
                <Spinner />
            ) : (
                <button className={styles["red-link"]} onClick={onClick}>
                    <TrashIcon color={`secondary`} />
                    {`Smazat soubor`}
                </button>
            )}
        </>
    );
};
