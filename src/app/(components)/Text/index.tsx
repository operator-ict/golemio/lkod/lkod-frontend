/* eslint-disable react/display-name */
import { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import styles from "./Text.module.scss";

interface ParagraphProps extends DetailedHTMLProps<HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement> {
    size?: "sm" | "regular" | "md" | "lg";
    centered?: boolean;
    className?: string;
}

export const Text = forwardRef<HTMLParagraphElement, ParagraphProps>(({ size, centered, className, ...restProps }, ref) => {
    return (
        <p
            className={`${!size ? styles["paragraph__regular"] : styles[`paragraph__${size}`]} ${
                centered ? styles.centered : ""
            } ${className ?? ""}`}
            {...restProps}
            ref={ref}
        />
    );
});
