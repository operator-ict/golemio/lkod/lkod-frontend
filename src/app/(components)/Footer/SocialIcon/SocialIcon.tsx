import Image from "next/image";
import Link from "next/link";
import React, { FC, memo } from "react";

import styles from "./SocialIcon.module.scss";

type SocialIcon = {
    link: string;
    url: string;
    label: string;
    isLogo?: boolean;
};

const SocialIcon: FC<SocialIcon> = ({ link, url, label, isLogo }) => {
    return (
        <Link href={link} className={`${styles["social-icon"]} ${isLogo ? styles["logo-icon"] : ""}`}>
            <Image src={url} alt={label} />
        </Link>
    );
};

export default memo(SocialIcon);
