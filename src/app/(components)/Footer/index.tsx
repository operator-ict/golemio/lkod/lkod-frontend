"use client";
import Link from "next/link";
import { Session } from "next-auth";
import { signOut } from "next-auth/react";
import React, { FC } from "react";

import { Logo } from "@/components/configurable/Logo";
import { Socials } from "@/components/Footer/Socials";
import HorizontalLine from "@/components/HorizontalLine";
import text from "@/textContent/cs.json";

import { LoginIcon } from "../icons/LoginIcon";
import { LogOutIcon } from "../icons/LogOutIcon";
import styles from "./Footer.module.scss";
import Logos from "./Logos/Logos";

interface FooterProps {
    url: string;
    showCatalogLink: string;
    session: Session | null;
}
const Conditions: FC<FooterProps> = ({ url, showCatalogLink, session }) => {
    return (
        <div className={styles.conditions}>
            {showCatalogLink !== "false" && (
                <Link className={styles["red-link"]} href={url}>
                    {text.footer.linkToCatalogue}
                </Link>
            )}
            {!session?.user?.email ? (
                <>
                    <Link className={styles["red-link"]} href={"/forgot-password"}>
                        {text.password.forgottenPassword}
                    </Link>
                    <Link className={styles["red-link"]} href={"/login"}>
                        {text.login.login}
                        <LoginIcon color={`secondary`} style={{ minWidth: "1rem", height: "1rem" }} />
                    </Link>
                </>
            ) : (
                <>
                    <Link className={styles["red-link"]} href={"/change-password"}>
                        {text.password.changePassword}
                    </Link>
                    <Link className={styles["red-link"]} href={"/instructions/guidance"}>
                        {text.footer.instruction}
                    </Link>
                    <Link
                        href={"/signout"}
                        className={styles["red-link"]}
                        onClick={() => signOut({ callbackUrl: "/signout", redirect: false })}
                    >
                        {text.login.logMeOut}
                        <LogOutIcon color={`secondary`} style={{ minWidth: "1rem", height: "1rem" }} />
                    </Link>
                </>
            )}
        </div>
    );
};

const Footer: FC<FooterProps> = ({ url, showCatalogLink, session }) => {
    return (
        <footer className={styles.footer}>
            <div className={styles["container"]}>
                <Logos />
                <Socials />
                <HorizontalLine className={styles["top-line"]} />
            </div>
            <div className={styles["container"]}>
                <Conditions url={url} showCatalogLink={showCatalogLink} session={session} />
                <HorizontalLine className={styles["bottom-line"]} />
                <div className={styles["logo-container"]}>
                    <Logo alt={text.configurable.logoOwner} footer src={""} />
                </div>
            </div>
        </footer>
    );
};

export default Footer;
