import React, { ReactNode } from "react";

import Button from "@/components/Button";

const ButtonInfo = ({ infoText, children }: { infoText: string; children: ReactNode }) => {
    return (
        <Button color="primary-light-outline-info">
            {children}
            {infoText}
        </Button>
    );
};

export default ButtonInfo;
