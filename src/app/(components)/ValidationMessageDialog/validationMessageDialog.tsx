import React, { FC } from "react";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import HorizontalLine from "@/components/HorizontalLine";
import { CloseIcon } from "@/components/icons/CloseIcon";
import { Modal } from "@/components/Modal";
import { Text } from "@/components/Text";
import text from "@/textContent/cs.json";
import { IDatasetValidationData } from "@/types/ValidatedDatasetInterface";

import styles from "./validationMessageDialog.module.scss";

export const ValidationMessageDialog: FC<{
    isOpen: boolean;
    closeDialog: () => void;
    validationResultData: IDatasetValidationData | null;
}> = ({ isOpen, closeDialog, validationResultData }) => {
    return (
        <Modal show={isOpen} onClose={closeDialog} label={`Chybějící metadata`} headerText={validationResultData?.label ?? ""}>
            <>
                <HorizontalLine />
                <article className={styles["validator-message_body"]}>
                    <Heading tag="h1" type={`h5`}>{`Metadata`}</Heading>
                    <div
                        tabIndex={0}
                        className={`${styles["validator-message_list"]} ${styles["validator-message_list-overflow"]}`}
                    >
                        {validationResultData?.result.messages &&
                            validationResultData.result.messages.map((message, i) => {
                                return (
                                    <div key={i} className={styles["validator-message_list-line"]}>
                                        <Text>{message.substring(0, message.indexOf(" "))}</Text>
                                        <Text>{message.substring(message.indexOf(" ") + 1)}</Text>
                                    </div>
                                );
                            })}
                    </div>
                    <div className={styles["validator-message_actions"]}>
                        <Button color={`secondary`} onClick={closeDialog} label={text.close}>
                            <CloseIcon color={`tertiary`} />
                        </Button>
                    </div>
                </article>
            </>
        </Modal>
    );
};
