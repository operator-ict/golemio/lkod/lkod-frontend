import React, { FC, ReactNode, useState } from "react";

import Button from "@/components/Button";
import { Card } from "@/components/Card";
import { RollBackIcon } from "@/components/icons/RollBackIcon";
import { TrashIcon } from "@/components/icons/TrashIcon";
import { Modal } from "@/components/Modal";
import TextField from "@/components/TextField";
import text from "@/textContent/cs.json";
import { useHighlightedName } from "@/utils/getHighlightedName";

import styles from "./DeleteDialog.module.scss";

export const DeleteDialog: FC<{
    isOpen: boolean;
    name?: string | null;
    titleIcon: ReactNode;
    titleText: string;
    subtitleText?: string | null;
    deleteButtonText: string;
    onConfirm: () => void;
    closeDialog: () => void;
}> = ({ isOpen, name, onConfirm, closeDialog, titleIcon, titleText, subtitleText, deleteButtonText }) => {
    const { getHighlightedName } = useHighlightedName();
    const [typed, setTyped] = useState("");

    return (
        <Modal
            show={isOpen}
            onClose={closeDialog}
            label={titleText}
            headerText={subtitleText ?? ""}
            headerWarning={text.deleteConfirmationWarning}
        >
            <Card className={styles.item}>
                {titleIcon}
                {getHighlightedName(name)}
            </Card>
            {name && (
                <TextField label={"Pro potvrzení vepište název."} value={typed} onChange={(e) => setTyped(e.target.value)} />
            )}
            <div className={styles.actions}>
                <Button color="outline-gray" onClick={closeDialog}>
                    <RollBackIcon color={`gray`} />
                    {text.rollBack}
                </Button>
                <Button
                    color={`secondary`}
                    onClick={() => {
                        closeDialog();
                        onConfirm();
                    }}
                    disabled={!!name && typed != name}
                >
                    <TrashIcon color="tertiary" />
                    {deleteButtonText}
                </Button>
            </div>
        </Modal>
    );
};

export default DeleteDialog;
