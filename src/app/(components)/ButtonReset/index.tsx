import { DetailedHTMLProps, HTMLAttributes } from "react";

import styles from "./ButtonReset.module.scss";

type Colors = "primary" | "secondary";

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    color?: Colors;
    label?: string;
    hideLabel?: boolean;
    end?: boolean;
    className?: string;
}

export const ButtonReset = ({ className, color, label, hideLabel, onClick, end }: Props) => {
    return (
        <button
            aria-label={label}
            className={`${styles["reset-button"]} ${color ? styles[`reset-button_color-${color}`] : ""} ${
                end ? styles["reset-button_end"] : ""
            } ${className ?? ""}`}
            onClick={onClick}
            type="reset"
        >
            {!hideLabel && <p>{label}</p>} &times;
        </button>
    );
};
