"use client";
/* eslint-disable react/display-name */

import React, { forwardRef, HTMLAttributes } from "react";
import { DetailedHTMLProps } from "react";

import { useDatasetListDownload } from "@/(hooks)/useDatasetListDownload";

import Button from "../Button";
import { FileDownloadIcon } from "../icons/FileDownloadIcon";
import styles from "./ResultLine.module.scss";

interface ResultProps extends DetailedHTMLProps<HTMLAttributes<HTMLSpanElement>, HTMLSpanElement> {
    result: number;
    foundPlurals: string[]; // [{0,5+}, 1, {2,3,4}]
    plurals: string[]; // [{0,5+}, 1, {2,3,4}]
    showDownloadDatasets?: boolean;
}

export const ResultLine = forwardRef<HTMLSpanElement, ResultProps>((props, ref) => {
    const { result, showDownloadDatasets, plurals, foundPlurals } = props;
    const { getDatasetList } = useDatasetListDownload();
    const pluralIndex = result > 4 || result === 0 ? 0 : result > 1 ? 2 : 1;

    return (
        <div className={styles["result-line_wrapper"]}>
            <span
                className={styles["result-line"]}
                ref={ref}
            >{`${foundPlurals[pluralIndex]} ${result} ${plurals[pluralIndex]}`}</span>
            {showDownloadDatasets && (
                <Button
                    color="outline-gray"
                    label="Stáhnout seznam datových sad"
                    onClick={() => {
                        getDatasetList();
                    }}
                >
                    <FileDownloadIcon color="black" />
                </Button>
            )}
        </div>
    );
});
