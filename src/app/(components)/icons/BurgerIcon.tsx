import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const BurgerIcon = ({ color, width = "1rem", height = "0.75rem", ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M0 1c0-.531.438-1 1-1h12c.531 0 1 .469 1 1 0 .563-.469 1-1 1H1c-.563 0-1-.438-1-1Zm0 5c0-.531.438-1
            1-1h12c.531 0 1 .469 1 1 0 .563-.469 1-1 1H1c-.563 0-1-.438-1-1Zm13 6H1c-.563 0-1-.438-1-1 0-.531.438-1 
            1-1h12c.531 0 1 .469 1 1 0 .563-.469 1-1 1Z"
        />
    </svg>
);
