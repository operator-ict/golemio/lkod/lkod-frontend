import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./ChevronIcon.module.scss";

export const ChevronIcon = ({
    color,
    direction,
    width = "9px",
    height = "14px",
    ...restProps
}: IiCon & SVGProps<SVGSVGElement>) => {
    return (
        <svg
            className={`${styles[`icon_${direction}`]} ${styles[`icon_color-${color}`]}`}
            style={{ width: width, height: height }}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox={`0 0 9 14`}
            {...restProps}
        >
            <path strokeLinecap="square" strokeWidth="2" d="M2.05 2l5 5-5 5"></path>
        </svg>
    );
};
