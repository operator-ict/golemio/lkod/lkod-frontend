import React, { SVGProps } from "react";

export const HomeIcon = ({ width = "1rem", height = "1rem", ...restProps }: SVGProps<SVGSVGElement>) => (
    <svg style={{ width: width, height: height }} fill="none" xmlns="http://www.w3.org/2000/svg" {...restProps}>
        <g filter="url(#a)">
            <path
                d="M15.917 14.793v-7.04c0-.475 0-.712-.06-.93a1.666 1.666 0 0 
                0-.261-.54c-.134-.183-.32-.33-.693-.624l-5.25-4.144c-.59-.466-.885-.7-1.212-.789a1.667 1.667 0
                0 0-.88 0c-.327.09-.622.323-1.213.789l-5.25 4.144c-.372.294-.558.441-.692.625a1.667 1.667 0 0
                0-.261.538c-.061.219-.061.456-.061.93v7.042c0 .776 0 1.164.127 1.47.169.41.493.733.902.903.306.127.694.127 
                1.471.127.777 0 1.165 0 1.471-.127.409-.17.733-.494.902-.902.127-.307.127-.695.127-1.472v-1.916c0-.934 
                0-1.4.182-1.757.16-.313.414-.568.728-.728.357-.182.823-.182 1.757-.182h.5c.933 0 1.4 0 
                1.756.182.314.16.569.415.729.728.181.357.181.823.181 1.757v1.916c0 
                .777 0 1.165.127 1.472.17.408.494.732.902.902.306.127.695.127 1.471.127.777 
                0 1.165 0 1.472-.127.408-.17.732-.494.902-.902.126-.307.126-.695.126-1.472Z"
                fill="url(#b)"
                fillOpacity={0.4}
            />
        </g>
        <defs>
            <linearGradient id="b" x1={15.957} y1={0.667} x2={-0.652} y2={16.523} gradientUnits="userSpaceOnUse">
                <stop stopColor="#96AAB7" />
                <stop offset={1} stopColor="#728896" />
            </linearGradient>
            <filter
                id="a"
                x={0.084}
                y={0.667}
                width={15.834}
                height={20.627}
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
            >
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                <feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                <feOffset dy={4} />
                <feGaussianBlur stdDeviation={2} />
                <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
                <feColorMatrix values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0" />
                <feBlend in2="shape" result="effect1_innerShadow_787_8651" />
            </filter>
        </defs>
    </svg>
);
