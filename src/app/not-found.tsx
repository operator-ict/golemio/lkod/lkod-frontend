"use client";

import { useRouter } from "next/navigation";
import { NextPage } from "next/types";
import React from "react";

import { CardInfo } from "@/components/CardInfo";
import ColorLink from "@/components/ColorLink";
import { Heading } from "@/components/Heading";
import { NoResultIcon } from "@/components/icons/NoResultIcon";

import classes from "./FourOhFour.module.scss";

const FourOhFour: NextPage = () => {
    const router = useRouter();

    return (
        <main className={classes.main}>
            <div className={classes.container}>
                <CardInfo className={classes["four-oh-four"]}>
                    <NoResultIcon />
                    <Heading tag={`h1`} type={`h3`}>
                        404 - Bohužel, stránka neexistuje.
                    </Heading>
                    <ColorLink linkText="Zpět" onClick={() => router.back()} direction={`left`} center />
                </CardInfo>
            </div>
        </main>
    );
};

export default FourOhFour;
