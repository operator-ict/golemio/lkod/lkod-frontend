"use client";
import { useRouter } from "next/navigation";
import React, { ChangeEvent, SyntheticEvent, useState } from "react";

import Button from "@/components/Button";
import { Card } from "@/components/Card";
import ColorLink from "@/components/ColorLink";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { EyeIcon } from "@/components/icons/Eye";
import { EyeSlashIcon } from "@/components/icons/Eye-slash";
import StatusPage from "@/components/StatusPage";
import TextField from "@/components/TextField";
import styles from "@/styles/Login.module.scss";
import text from "@/textContent/cs.json";

import { authorizationServices } from "../../(services)/authorization-service";

const ChangePassword = () => {
    const [loginError, setLoginError] = useState("");
    const [passwordFields, setPasswordFields] = useState({
        typeCurrentPassword: "password",
        typeNewPassword: "password",
        typePasswordRepeat: "password",
        currentPassword: "",
        newPassword: "",
        passwordRepeat: "",
    });
    const [changeSuccess, setChangeSuccess] = useState<string | null>(null);

    const router = useRouter();

    const onSubmit = async (e: SyntheticEvent) => {
        e.preventDefault();

        if (passwordFields.newPassword !== passwordFields.passwordRepeat) {
            setLoginError(`${text.errors.passwordMismatch}`);
            return;
        }

        try {
            const response = await authorizationServices.changePassword({
                currentPassword: passwordFields.currentPassword,
                newPassword: passwordFields.newPassword,
            });

            if (response.status >= 200 && response.status <= 299) {
                setChangeSuccess(response.status.toString());
            } else {
                throw { status: response.status };
            }
        } catch (error) {
            setLoginError(getErrorKey(error.status));
            setChangeSuccess(null);
        }
    };

    const getErrorKey = (reason: number | string) => {
        switch (reason) {
            case 400:
                return `${text.errors.emptyfields}`;
            case 401:
                return `${text.errors.invalidLogin}`;
            default:
                return `${text.errors.unknown}`;
        }
    };

    return (
        <Container className={`${styles["login-container"]}`}>
            {changeSuccess ? (
                <StatusPage pageLabel={text.passwordChanged} massage={text.passwordChangedMessage} />
            ) : (
                <>
                    <div className={`${styles["login-link-back"]}`}>
                        <ColorLink linkText={text.back} onClick={() => router.back()} direction={`left`} start />
                    </div>
                    <div className={`${styles["container"]}`}>
                        <Card className={`${styles["login-box"]}`}>
                            <Heading tag={`h1`} type={`h3`} className={`${styles["login-title"]}`}>
                                {text.password.changePassword}
                            </Heading>

                            <form className={`${styles["form-wrapper"]}`} onSubmit={onSubmit}>
                                <TextField
                                    name="password"
                                    type={passwordFields.typeCurrentPassword}
                                    placeholder={text.password.inputOldPassword}
                                    label={text.password.oldPassword}
                                    value={passwordFields.currentPassword}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                                        setPasswordFields((prevState) => ({
                                            ...prevState,
                                            currentPassword: e.target.value,
                                        }))
                                    }
                                >
                                    {passwordFields.typeCurrentPassword === "password" ? (
                                        <EyeIcon
                                            color={`primary-light`}
                                            onClick={() =>
                                                setPasswordFields((prevState) => ({
                                                    ...prevState,
                                                    typeCurrentPassword: "text",
                                                }))
                                            }
                                        />
                                    ) : (
                                        <EyeSlashIcon
                                            color={`primary-light`}
                                            onClick={() =>
                                                setPasswordFields((prevState) => ({
                                                    ...prevState,
                                                    typeCurrentPassword: "password",
                                                }))
                                            }
                                        />
                                    )}
                                </TextField>
                                {changeSuccess}
                                <TextField
                                    name="password"
                                    type={passwordFields.typeNewPassword}
                                    placeholder={text.password.inputNewPassword}
                                    label={text.password.newPassword}
                                    value={passwordFields.newPassword}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                                        setPasswordFields((prevState) => ({
                                            ...prevState,
                                            newPassword: e.target.value,
                                        }))
                                    }
                                >
                                    {passwordFields.typeNewPassword === "password" ? (
                                        <EyeIcon
                                            color={`primary-light`}
                                            onClick={() =>
                                                setPasswordFields((prevState) => ({
                                                    ...prevState,
                                                    typeNewPassword: "text",
                                                }))
                                            }
                                        />
                                    ) : (
                                        <EyeSlashIcon
                                            color={`primary-light`}
                                            onClick={() =>
                                                setPasswordFields((prevState) => ({
                                                    ...prevState,
                                                    typeNewPassword: "password",
                                                }))
                                            }
                                        />
                                    )}
                                </TextField>
                                <TextField
                                    name="password"
                                    type={passwordFields.typePasswordRepeat}
                                    placeholder={text.password.repeatNewPassword}
                                    label={text.password.repeatNewPassword}
                                    value={passwordFields.passwordRepeat}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                                        setPasswordFields((prevState) => ({
                                            ...prevState,
                                            passwordRepeat: e.target.value,
                                        }))
                                    }
                                >
                                    {passwordFields.typePasswordRepeat === "password" ? (
                                        <EyeIcon
                                            color={`primary-light`}
                                            onClick={() =>
                                                setPasswordFields((prevState) => ({
                                                    ...prevState,
                                                    typePasswordRepeat: "text",
                                                }))
                                            }
                                        />
                                    ) : (
                                        <EyeSlashIcon
                                            color={`primary-light`}
                                            onClick={() =>
                                                setPasswordFields((prevState) => ({
                                                    ...prevState,
                                                    typePasswordRepeat: "password",
                                                }))
                                            }
                                        />
                                    )}
                                </TextField>
                                <div className={`${styles["input-button"]}`}>
                                    <Button color="secondary" type="submit" label={text.change} />
                                    <div className={`${styles["error"]}`}>{loginError}</div>
                                </div>
                            </form>
                        </Card>
                    </div>
                </>
            )}
        </Container>
    );
};

export default ChangePassword;
