import { redirect } from "next/navigation";
import { NextPage } from "next/types";
import { getServerSession } from "next-auth";
import React from "react";
import { config } from "src/config";

import { Card } from "@/components/Card";
import ColorLink from "@/components/ColorLink";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import LoginForm from "@/components/LoginForm";
import styles from "@/styles/Login.module.scss";
import text from "@/textContent/cs.json";

import { authOptions } from "../../api/auth/[...nextauth]/authOptions";

const PAGE_LABEL = "Přihlášení do Open Data";

const Login: NextPage = async () => {
    const session = await getServerSession(authOptions);

    const isLoggedIn = session?.isLoggedIn;

    if (isLoggedIn) {
        redirect(`${config.NEXTAUTH_URL}`);
    }

    return (
        <Container className={`${styles["login-container"]}`}>
            <div className={`${styles["login-link-back"]}`}>
                <ColorLink linkText={text.back} back direction={`left`} start />
            </div>
            <div className={`${styles["container"]}`}>
                <Card className={`${styles["login-box"]}`}>
                    <Heading tag={`h1`} type={`h3`} className={`${styles["login-title"]}`}>
                        {PAGE_LABEL}
                    </Heading>
                    <LoginForm />
                </Card>
            </div>
        </Container>
    );
};

export default Login;
