import Link from "next/link";
import React from "react";

import Button from "@/components/Button";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading/index";
import styles from "@/styles/SignOut.module.scss";

const StatusPage = () => {
    const PAGE_LABEL = "Heslo bylo změněno";

    return (
        <Container>
            <div className={styles["acsd-container"]}>
                <Heading tag={`h1`} className={styles["acsd-title"]}>
                    {PAGE_LABEL}
                </Heading>
                <p> Úspěšně jste změnili heslo k účtu a můžete se přihlásit</p>
                <Link href="/login">
                    <Button className={styles["acsd-button"]} color="secondary" type="submit" label={"Zpět na příhlášení"} />
                </Link>
            </div>
        </Container>
    );
};

export default StatusPage;
