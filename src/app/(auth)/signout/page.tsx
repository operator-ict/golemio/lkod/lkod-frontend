"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { NextPage } from "next/types";
import { signOut } from "next-auth/react";
import { useEffect, useState } from "react";

import Button from "@/components/Button";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading/index";
import styles from "@/styles/SignOut.module.scss";

const SignOut: NextPage = () => {
    const router = useRouter();
    const searchParams = useSearchParams();

    const isExpired = searchParams.get("expired");
    const [pageLabel] = useState(isExpired ? "Platnost vaší relace vypršela" : "Byli jste odhlášeni");
    const [pageSubtitle] = useState(
        isExpired
            ? "Vypršela platnost Vašeho připojení, prosím přihlaste se znovu"
            : "Z bezpečnostních důvodů doporučujeme zavřít okno prohlížeče"
    );

    useEffect(() => {
        // Automatically sign the user out
        signOut({ callbackUrl: "/signout", redirect: false });
    }, []);

    const handleClick = () => {
        router.push("/login");
    };

    return (
        <Container>
            <div className={styles["acsd-container"]}>
                <Heading tag={`h1`} className={styles["acsd-title"]}>
                    {pageLabel}
                </Heading>
                <p>{pageSubtitle}</p>
                <Button
                    className={styles["acsd-button"]}
                    color="secondary"
                    type="submit"
                    label={"Znovu se přihlásit"}
                    onClick={handleClick}
                />
            </div>
        </Container>
    );
};

export default SignOut;
