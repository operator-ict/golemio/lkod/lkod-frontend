import { CreateSession } from "@/types/Dataset";

import { ApiClient } from "./api-client";
export interface CreateSessionRequest {
    createSessionInput: CreateSessionInput;
}
export interface CreateSessionRequest {
    createSessionInput: CreateSessionInput;
}
export interface CreateSessionInput {
    datasetId: string;
}

export interface CreateSessionOutput {
    id: string;
    datasetId: string;
    userId: number;
    createdAt?: Date | string | undefined;
    expiresIn: number;
}

export class SessionsService {
    async createSession(body: CreateSessionRequest): Promise<CreateSession> {
        const response = await ApiClient.post<CreateSessionOutput>("sessions", body.createSessionInput);

        return this.transformSessionOutput(response.data as CreateSession);
    }

    private transformSessionOutput = (json: CreateSession): CreateSession => {
        if (json === undefined || json === null) {
            return json;
        }
        return {
            id: json.id,
            datasetId: json.datasetId,
            userId: json.userId,
            createdAt: json.createdAt,
            expiresIn: json.expiresIn,
        };
    };
}
