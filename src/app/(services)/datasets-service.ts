import { DatasetFilters, DatasetsFiltered } from "@/types/Dataset";
import { IFilter } from "@/types/FilterInterface";
import { IFormats, IKeywords, IPublisher, IStatus, IThemes } from "@/types/LookupTypes";
import { toQueryString } from "@/utils/helpers";

import { ApiClient, ResponseType } from "./api-client";

export class DatasetsServices {
    async getFilteredDatasets(requestParameters: DatasetFilters, headers: HeadersInit): Promise<ResponseType<DatasetsFiltered>> {
        const isValidLimit = typeof requestParameters.limit === "string" || typeof requestParameters.limit === "number";
        const limitParam = isValidLimit ? `&limit=${requestParameters.limit.toString()}` : "";
        const offsetParam = requestParameters.offset ? `&offset=${requestParameters.offset}` : "";
        const filter = requestParameters.filter ? toQueryString(requestParameters.filter) : "";
        const searchString = requestParameters.searchString
            ? `&searchString=${encodeURIComponent(requestParameters.searchString)}`
            : "";
        const path = `datasets${filter}${searchString}${limitParam}${offsetParam}`;
        return ApiClient.get<DatasetsFiltered>(path, headers);
    }

    async getDatasetsListFile(): Promise<ResponseType<Blob>> {
        return ApiClient.get<Blob>("datasets.csv", { "Content-Type": "text/csv" });
    }

    async getStatusesDataset(filter: IFilter): Promise<IStatus[]> {
        const response = await ApiClient.get<IStatus[]>(`lookup/statuses${toQueryString(filter)}`);
        return response.data ?? [];
    }

    async getPublishers(filter: IFilter): Promise<IPublisher[]> {
        const response = await ApiClient.get<IPublisher[]>(`lookup/publishers${toQueryString(filter)}`);
        return response.data ?? [];
    }

    async getThemes(filter: IFilter): Promise<IThemes[]> {
        const response = await ApiClient.get<IThemes[]>(`lookup/themes${toQueryString(filter)}`);
        return response.data ?? [];
    }

    async getKeywords(filter: IFilter): Promise<IKeywords[]> {
        const response = await ApiClient.get<IKeywords[]>(`lookup/keywords${toQueryString(filter)}`);
        return response.data ?? [];
    }

    async getFormats(filter: IFilter): Promise<IFormats[]> {
        const response = await ApiClient.get<IFormats[]>(`lookup/formats${toQueryString(filter)}`);
        return response.data ?? [];
    }
}

const datasetsServices = new DatasetsServices();
export { datasetsServices };
