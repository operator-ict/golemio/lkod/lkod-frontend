import { getServerSession } from "next-auth/next";
import { config } from "src/config";

import { getLogger } from "@/logging/logger";

import { getErrorMessage } from "../(utils)/getErrorMessage";
import { authOptions } from "../api/auth/[...nextauth]/authOptions";

export type ResponseType<T> = {
    count: string | null;
    data: T | null;
    status: number;
    error?: string;
};

const logger = getLogger("api-client");

export class ApiClient {
    static async request<T>(
        method: "GET" | "POST" | "DELETE" | "PATCH",
        requestUrl: string,
        body?: unknown,
        headersInit?: HeadersInit
    ): Promise<ResponseType<T>> {
        const startTime = Date.now();
        const fullUrl = `${config.BACKEND_URL}/${requestUrl}`;
        let logData: Record<string, unknown> = {
            provider: "server",
            class: "ApiClient",
            method,
            fullUrl,
        };

        try {
            const session = await getServerSession(authOptions);
            const token = session?.token;
            const defaultHeaders: HeadersInit = {
                ...(!(body instanceof FormData) && { "Content-Type": "application/json" }),
                ...(token && { Authorization: `Bearer ${token}` }),
            };

            const headers = { ...defaultHeaders, ...headersInit };
            logData = {
                ...logData,
            };

            const options: RequestInit = {
                method,
                headers,
            };

            if (body && method !== "GET") {
                options.body = body instanceof FormData ? body : JSON.stringify(body);
                logData = { ...logData, requestBody: body };
            }

            const response = await fetch(fullUrl, options);

            const count = response.headers.get("X-Total-Count");
            logData = { ...logData, count };

            if (response.ok) {
                let responseData = null;
                const contentType = response.headers.get("Content-type");

                if (contentType) {
                    if (contentType.includes("text/csv")) {
                        responseData = await response.blob();
                    } else if (contentType.includes("application/json")) {
                        if (response.status !== 204 && response.status !== 202) {
                            responseData = await response.json();
                        }
                    } else if (contentType.includes("text/plain")) {
                        responseData = await response.text();
                    }
                }

                const endTime = Date.now();
                logger.debug({
                    ...logData,
                    duration: `${endTime - startTime}ms`,
                    message: `Request successful - status: ${response.status}`,
                });

                return {
                    status: response.status,
                    count,
                    data: responseData as T,
                };
            }

            const errorResponse = await response.json().catch(() => null); // Handle parse errors gracefully
            const endTime = Date.now();

            logger.debug({
                ...logData,
                duration: `${endTime - startTime}ms`,
                message: `Request failed - status: ${response.status}, error: ${JSON.stringify(errorResponse)}`,
            });

            return {
                status: response.status,
                count,
                data: null,
                error: errorResponse || { message: "Failed to parse server response" },
            };
        } catch (error) {
            const endTime = Date.now();
            logger.error({
                ...logData,
                duration: `${endTime - startTime}ms`,
                message: `${getErrorMessage(error)}, status: ${error.status}`,
            });
            return {
                status: error.status,
                count: null,
                data: null,
                error: getErrorMessage(error),
            };
        }
    }

    static async get<T>(requestUrl: string, headers?: HeadersInit): Promise<ResponseType<T>> {
        return this.request<T>("GET", requestUrl, undefined, headers);
    }

    static post<T>(requestUrl: string, body: unknown, headers?: HeadersInit) {
        return this.request<T>("POST", requestUrl, body, headers);
    }

    static delete<T>(requestUrl: string, body?: unknown) {
        return this.request<T>("DELETE", requestUrl, body);
    }

    static patch<T>(requestUrl: string, body: unknown) {
        return this.request<T>("PATCH", requestUrl, body);
    }
}
