"use server";

import { ApiClient, ResponseType } from "./api-client";

/**
 * Availability of storage
 */
export async function getFilesStatus(): Promise<ResponseType<string>> {
    return ApiClient.get<string>("status/files");
}
