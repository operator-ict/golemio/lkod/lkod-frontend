import { AuthorizationServices } from "./authorization-service";
import { DatasetsServices } from "./datasets-service";
import { FormDataService } from "./form-data-service";
import { OrganizationsService } from "./organizations-service";
import { SessionsService } from "./session-service";
import { getFilesStatus } from "./status-service";
import { UsersService } from "./users-service";

export type ApiType = {
    authorizationApi: AuthorizationServices;
    datasetsApi: DatasetsServices;
    formDataApi: FormDataService;
    organizationsApi: OrganizationsService;
    sessionsApi: SessionsService;
    statusApi: {
        getFilesStatus: typeof getFilesStatus;
    };
    usersApi: UsersService;
};

export const initApis = (): ApiType => {
    return {
        authorizationApi: new AuthorizationServices(),
        datasetsApi: new DatasetsServices(),
        formDataApi: new FormDataService(),
        organizationsApi: new OrganizationsService(),
        sessionsApi: new SessionsService(),
        statusApi: {
            getFilesStatus,
        },
        usersApi: new UsersService(),
    };
};

export const ApiClient = initApis();
