"use client";
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-empty-function */
import { useRouter } from "next/navigation";
import { Session } from "next-auth";
import { signOut } from "next-auth/react";
import { ParsedUrlQuery, stringify } from "querystring";
import { createContext, FC, ReactNode, useCallback, useMemo, useState } from "react";

import { getLogger } from "@/logging/logger";
import { ApiClient } from "@/services/initApi";
import { Dataset, UpdateDatasetInputStatus } from "@/types/Dataset";

import { dataServiceClient } from "../(services)/client/datasets-service-client";

const logger = getLogger("DatasetsContext");

type DatasetsState = {
    isCreateLoading: boolean;
    isEditLoading: boolean;
    datasetsError: string | null;
    datasetsCreateError: string | null;
};

type DatasetsActions = {
    deleteDataset: (datasetId: string) => void;
    createDataset: (organizationId: number) => void;
    editDataset: (dataset: Dataset) => void;
    updateDataset: (dataset: Dataset, visible: boolean, userId?: number) => void;
};

const defaultState = {
    datasets: [],
    isCreateLoading: false,
    isEditLoading: false,
    datasetsError: null,
    datasetsCreateError: null,
};

export const DatasetsContext = createContext<DatasetsState & DatasetsActions>({
    ...defaultState,
    editDataset: () => {},
    createDataset: () => {},
    deleteDataset: () => {},
    updateDataset: () => {},
});

export const DatasetsProvider: FC<{ children: ReactNode; externalUrl: string; returnUrl: string; session: Session | null }> = ({
    children,
    externalUrl,
    returnUrl,
    session,
}) => {
    const [datasets] = useState<Dataset[]>(defaultState.datasets);
    const [isCreateLoading, setCreateLoading] = useState<boolean>(defaultState.isCreateLoading);
    const [isEditLoading, setEditLoading] = useState<boolean>(defaultState.isEditLoading);
    const [datasetsError, setDatasetsError] = useState<string | null>(defaultState.datasetsError);
    const [datasetsCreateError, setDatasetsCreateError] = useState<string | null>(defaultState.datasetsCreateError);
    const { sessionsApi } = ApiClient;

    const router = useRouter();
    const onError = useCallback((reason: { status: number }) => {
        if (reason?.status === 401) {
            logger.error({
                provider: "client",
                component: "DatasetsProvider",
                status: reason.status,
                message: "Unauthorized access detected, signing out.",
            });
            signOut();
            return;
        }
        logger.error({
            provider: "client",
            component: "DatasetsProvider",
            status: reason.status,
            message: reason,
        });
        setDatasetsError("errors.unknown");
    }, []);

    const goToForm = useCallback(
        async (dataset: Dataset, accessToken: string, query?: ParsedUrlQuery) => {
            try {
                const { id: sessionId } = await dataServiceClient.createSession({
                    createSessionInput: {
                        datasetId: dataset.id,
                    },
                });

                const form = document.createElement("form");
                form.action = `${externalUrl}?returnUrl=${encodeURI(returnUrl)}`;
                form.method = "POST";
                const queryString = query ? stringify(query) : "";

                const userInput = document.createElement("input");
                userInput.name = "userData";
                userInput.type = "hidden";
                userInput.value = JSON.stringify({
                    accessToken,
                    sessionId,
                    datasetId: dataset.id,
                    backlinkUrlPath: queryString && queryString.length > 0 ? "?" + queryString : "",
                });
                form.appendChild(userInput);

                if (dataset.data) {
                    const dataInput = document.createElement("input");
                    dataInput.name = "formData";
                    dataInput.type = "hidden";
                    dataInput.value = JSON.stringify(dataset.data);
                    form.appendChild(dataInput);
                }

                document.body.appendChild(form);
                form.submit();
            } catch (reason: any) {
                if (reason?.status === 401) {
                    logger.error({
                        provider: "client",
                        action: "goToForm",
                        component: "DatasetsProvider",
                        status: reason.status,
                        message: "Unauthorized goToForm error, signing out.",
                    });
                    signOut();
                    return;
                }
                logger.error({
                    provider: "client",
                    action: "goToForm",
                    component: "DatasetsProvider",
                    status: reason?.status,
                    message: `goToForm error: ${reason}`,
                });
            } finally {
                setEditLoading(false);
            }
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [sessionsApi]
    );

    const accessToken = session && session.token;

    const editDataset = useCallback(
        (dataset: Dataset) => {
            if (!accessToken) {
                logger.warn({
                    provider: "client",
                    action: "editDataset",
                    component: "DatasetsProvider",
                    message: "Access token not found, signing out.",
                });
                signOut();
                return;
            }

            setEditLoading(true);
            goToForm(dataset, accessToken);
        },
        [goToForm, accessToken]
    );

    const createDataset = useCallback(
        async (organizationId: number) => {
            if (!accessToken) {
                logger.warn({
                    component: "DatasetsProvider",
                    action: "createDataset",
                    message: "Access token not found, signing out.",
                });
                signOut();
                return;
            }

            try {
                setCreateLoading(true);
                setDatasetsCreateError(null);

                const newDataset = await dataServiceClient.createDataset({
                    createDatasetInput: {
                        organizationId,
                    },
                });
                goToForm(newDataset, accessToken);
            } catch (reason: any) {
                if (reason?.status === 401) {
                    logger.error({
                        provider: "client",
                        component: "DatasetsProvider",
                        action: "createDataset",
                        status: reason.status,
                        message: "Unauthorized createDataset error, signing out.",
                    });
                    signOut();
                    return;
                }
                logger.error({
                    provider: "client",
                    component: "DatasetsProvider",
                    action: "createDataset",
                    status: reason?.status,
                    message: "Error creating dataset.",
                });
                setDatasetsCreateError("errors.unknown");
            } finally {
                setCreateLoading(false);
            }
        },
        [accessToken, goToForm]
    );

    const deleteDataset = useCallback(
        (datasetId: string) => {
            dataServiceClient.deleteDataset({ datasetId }).finally(() => {
                router.refresh();
            });
        },
        [router]
    );

    const updateDataset = useCallback(
        (dataset: Dataset, visibility: boolean, userId?: number) => {
            setEditLoading(true);
            const statusInput = {
                status: visibility ? UpdateDatasetInputStatus.Published : UpdateDatasetInputStatus.Unpublished,
                userId,
            };

            dataServiceClient
                .updateDataset(dataset.id, statusInput)
                .then(() => {
                    router.refresh();
                })
                .catch(onError)
                .finally(() => {
                    setEditLoading(false);
                });
        },
        [onError, router]
    );

    const datasetsState = useMemo(() => {
        return {
            isEditLoading,
            isCreateLoading,
            datasetsError: datasetsError && datasetsError.toString(),
            datasetsCreateError: datasetsCreateError && datasetsCreateError.toString(),
            datasets,
            editDataset,
            createDataset,
            deleteDataset,
            updateDataset,
        };
    }, [
        isEditLoading,
        isCreateLoading,
        datasets,
        datasetsError,
        datasetsCreateError,
        editDataset,
        createDataset,
        deleteDataset,
        updateDataset,
    ]);

    return <DatasetsContext.Provider value={datasetsState}>{children}</DatasetsContext.Provider>;
};
